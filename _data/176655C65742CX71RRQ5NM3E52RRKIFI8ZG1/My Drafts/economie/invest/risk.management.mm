<map version="docear 1.1" project="176655C65742CX71RRQ5NM3E52RRKIFI8ZG1" project_last_home="file:/C:/cimpa/perso/3ilmaa/" dcr_id="1615456008291_387y834ykszrqrfg4xylidpm">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="risk.management" FOLDED="false" ID="ID_237610849" CREATED="1615456008253" MODIFIED="1615456008255">
<hook NAME="AutomaticEdgeColor" COUNTER="3"/>
<hook NAME="MapStyle">
    <properties show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="fixer des objectifs" POSITION="left" ID="ID_1062841973" CREATED="1615478259499" MODIFIED="1615478626691" MOVED="1615478627293">
<icon BUILTIN="full-1"/>
<edge COLOR="#0000ff"/>
<node TEXT="chiffre l&apos;objectif SMART &#xe0; atteindre" ID="ID_1534619051" CREATED="1615480156357" MODIFIED="1615480255218">
<node TEXT="specific" ID="ID_1547744494" CREATED="1615480255225" MODIFIED="1615480258404"/>
<node TEXT="mesurable" ID="ID_335269734" CREATED="1615480258904" MODIFIED="1615480263422"/>
<node TEXT="atteignable" ID="ID_1513745875" CREATED="1615480263599" MODIFIED="1615480267623"/>
<node TEXT="r&#xe9;alisable" ID="ID_792345443" CREATED="1615480268055" MODIFIED="1615480271367"/>
<node TEXT="temporel" ID="ID_1981506462" CREATED="1615480272293" MODIFIED="1615480276599"/>
</node>
</node>
<node TEXT="identifier les risques&#xa;---------------------------------------&#xa;pour lesquels nous ne pourrions&#xa;pas atteindre nos objectifs" POSITION="left" ID="ID_829169581" CREATED="1615478269788" MODIFIED="1615480363933" MOVED="1615478630170">
<icon BUILTIN="full-2"/>
<edge COLOR="#00ff00"/>
<node TEXT="retournement du march&#xe9;" ID="ID_947502395" CREATED="1615478645441" MODIFIED="1615478768447" LINK="#ID_1931369013"/>
<node TEXT="les &#xe9;motions/psychologie" ID="ID_671834531" CREATED="1615478653698" MODIFIED="1615478664870"/>
<node ID="ID_1489853748" CREATED="1615478802651" MODIFIED="1615479867913" LINK="#ID_189357358"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <a href="https://academy.binance.com/fr/articles/liquidity-explained">liquidit&#233; </a>
    </p>
  </body>
</html>

</richcontent>
<node TEXT="du march&#xe9;" ID="ID_324484489" CREATED="1615478837890" MODIFIED="1615478840285">
<node TEXT="il y a tjs des investisseurs pr&#xea;ts &#xe0; acheter/vendre" ID="ID_1937166581" CREATED="1615479191948" MODIFIED="1615479211474"/>
</node>
<node TEXT="de l&apos;actif" ID="ID_1818677288" CREATED="1615478841137" MODIFIED="1615478845044">
<node TEXT="actif facilement convertible en esp&#xe8;ces" ID="ID_294874301" CREATED="1615479213613" MODIFIED="1615479228962"/>
</node>
<node ID="ID_558414913" CREATED="1615479310905" MODIFIED="1615479442961"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <ul>
      <li>
        plus un march&#233; est liquide et plus on trouve facilement un acheteur/vendeur
      </li>
      <li>
        ainsi, pas besoin d'attendre longtemps, risquant une fluctuation du prix pour vendre/acheter
      </li>
      <li>
        il doit y avoir une activit&#233; commerciale intense
      </li>
      <li>
        le prix entre acheteur et vendeur ne doit pas &#234;tre trop &#233;cart&#233;
      </li>
    </ul>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="exposition &#xe0; un seul projet/actif/soci&#xe9;t&#xe9;" ID="ID_68154688" CREATED="1615479975557" MODIFIED="1615480018876" LINK="#ID_404272216"/>
<node TEXT="faillite d&apos;un projet/actif/soci&#xe9;t&#xe9;" ID="ID_1539424131" CREATED="1615479997571" MODIFIED="1615480018860" LINK="#ID_404272216"/>
<node TEXT="&#xe9;v&#xe8;nement g&#xe9;o-politique" ID="ID_803968166" CREATED="1615485456493" MODIFIED="1615485502967" LINK="#ID_1213251513"/>
</node>
<node TEXT="le process" POSITION="right" ID="ID_1457676334" CREATED="1615478254263" MODIFIED="1615478258429">
<edge COLOR="#ff0000"/>
<node TEXT="&#xe9;valuer les risques" ID="ID_521729334" CREATED="1615478275989" MODIFIED="1615478421334">
<icon BUILTIN="full-3"/>
</node>
<node TEXT="d&#xe9;finir les proc&#xe9;dures" ID="ID_721873654" CREATED="1615478323135" MODIFIED="1615478424212">
<icon BUILTIN="full-4"/>
<node TEXT="pour chaque niveau de risque&#xa;apporter la r&#xe9;ponse ad&#xe9;quate" ID="ID_935623074" CREATED="1615478721540" MODIFIED="1615478740248"/>
<node TEXT="stop-loss" ID="ID_1931369013" CREATED="1615478745572" MODIFIED="1615478749065"/>
<node TEXT="trader sur les gros volume" ID="ID_189357358" CREATED="1615479822005" MODIFIED="1615479852684" LINK="#ID_1489853748"/>
<node TEXT="diversification du protfolio" ID="ID_404272216" CREATED="1615479944678" MODIFIED="1615479952764"/>
<node TEXT="calendrier &#xe9;conomique" ID="ID_340965356" CREATED="1615485473811" MODIFIED="1615485502985" LINK="#ID_1213251513"/>
<node TEXT="news" ID="ID_1213251513" CREATED="1615485483859" MODIFIED="1615485487981"/>
</node>
<node TEXT="Surveiller" ID="ID_1172468547" CREATED="1615478386213" MODIFIED="1615478427237">
<icon BUILTIN="full-5"/>
</node>
</node>
</node>
</map>
