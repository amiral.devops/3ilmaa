<map version="docear 1.1" dcr_id="1616744135696_cbl6e92n6bfiyljmpin87p6h6" project="176655C65742CX71RRQ5NM3E52RRKIFI8ZG1" project_last_home="file:/C:/Users/60000215/Docear/projects/3ilmaa/">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="smartContract" FOLDED="false" ID="ID_144946278" CREATED="1616744135658" MODIFIED="1616744135663">
<hook NAME="AutomaticEdgeColor" COUNTER="2"/>
<hook NAME="MapStyle">
    <properties show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="langage" POSITION="right" ID="ID_815080418" CREATED="1616744141506" MODIFIED="1616744154664">
<edge COLOR="#ff0000"/>
<node TEXT="solidity" ID="ID_1761130963" CREATED="1616744168286" MODIFIED="1616744168286"/>
<node TEXT="viper" ID="ID_1503739330" CREATED="1616744170161" MODIFIED="1616744174897"/>
</node>
<node TEXT="framework" POSITION="right" ID="ID_393414135" CREATED="1616744155387" MODIFIED="1616744162684">
<edge COLOR="#0000ff"/>
<node TEXT="truffle" ID="ID_986461909" CREATED="1616744183760" MODIFIED="1616744186976"/>
<node TEXT="ganache" ID="ID_274146458" CREATED="1616744606850" MODIFIED="1616744609465"/>
<node TEXT="my Ether wallet" ID="ID_1800083628" CREATED="1616744609775" MODIFIED="1616744614788"/>
<node TEXT="Metamask" ID="ID_1258136425" CREATED="1616744615034" MODIFIED="1616744620704"/>
</node>
</node>
</map>
