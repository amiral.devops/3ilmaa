<map version="docear 1.1" dcr_id="1664700204166_6lkk4v1jb1hxzl9h3ze5sy98v" project="176655C65742CX71RRQ5NM3E52RRKIFI8ZG1" project_last_home="file:/home/yassine/Developpement/projects/3ilmaa/">
<!--To view this file, download Docear - The Academic Literature Suite from http://www.docear.org -->
<node TEXT="aws" FOLDED="false" ID="ID_1124301269" CREATED="1664700203975" MODIFIED="1664700203980">
<hook NAME="AutomaticEdgeColor" COUNTER="5"/>
<hook NAME="MapStyle">
    <properties show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="" POSITION="right" ID="ID_1952999326" CREATED="1664700712942" MODIFIED="1664700712944">
<edge COLOR="#00ffff"/>
</node>
<node TEXT="Essentials of Cloud computing" POSITION="right" ID="ID_1048358202" CREATED="1664700260575" MODIFIED="1664700292362">
<edge COLOR="#ff0000"/>
<node TEXT="Mod&#xe8;les" ID="ID_1110665785" CREATED="1664700260575" MODIFIED="1664700762647">
<node TEXT="Public" ID="ID_1093322374" CREATED="1664700260575" MODIFIED="1664700771263"/>
<node TEXT="priv&#xe9;" ID="ID_874023878" CREATED="1664700771726" MODIFIED="1664700777103"/>
<node TEXT="communautaire" ID="ID_316534049" CREATED="1664700777686" MODIFIED="1664700783943"/>
<node TEXT="hybrid" ID="ID_423729440" CREATED="1664700784382" MODIFIED="1664700786719"/>
</node>
<node TEXT="Services" ID="ID_1723631885" CREATED="1664700791366" MODIFIED="1664700807327">
<node TEXT="SaaS" ID="ID_430335693" CREATED="1664700260575" MODIFIED="1664700821991">
<node TEXT="Office 365" ID="ID_1129084397" CREATED="1664700260575" MODIFIED="1664704133994"/>
<node TEXT="GMail" ID="ID_603600503" CREATED="1664704134616" MODIFIED="1664704140089"/>
</node>
<node TEXT="PaaS" ID="ID_129021151" CREATED="1664700822438" MODIFIED="1664700824927">
<node TEXT="Runtime &#xe0; louer" ID="ID_1030906647" CREATED="1664700260575" MODIFIED="1664704150593"/>
<node TEXT="DB -&gt; mais il reste &#xe0; cr&#xe9;er les sch&#xe9;mas + tables" ID="ID_1678493508" CREATED="1664704151048" MODIFIED="1664704168378"/>
<node TEXT="on contr&#xf4;le ce que l&apos;on d&#xe9;ploit mais pas comment la platforme est d&#xe9;ploy&#xe9;e" ID="ID_281050412" CREATED="1664704176456" MODIFIED="1664704202417"/>
</node>
<node TEXT="IaaS" ID="ID_29019529" CREATED="1664700825270" MODIFIED="1664700828391"/>
<node TEXT="FaaS" ID="ID_4134385" CREATED="1664700828790" MODIFIED="1664700833176">
<node TEXT="Lambda" ID="ID_915106506" CREATED="1664700260575" MODIFIED="1664700845415"/>
</node>
</node>
<node TEXT="Bases du cloud" ID="ID_1234612200" CREATED="1664700879702" MODIFIED="1664700988943">
<node TEXT="Virtualisation" ID="ID_389184786" CREATED="1664700260575" MODIFIED="1664700894687"/>
<node TEXT="Contain&#xe9;risation" ID="ID_76522562" CREATED="1664700937374" MODIFIED="1664704318106"/>
<node TEXT="Orchestration" ID="ID_1581398593" CREATED="1664700941910" MODIFIED="1664700945487"/>
</node>
<node TEXT="Cloud native" ID="ID_649770321" CREATED="1664700989518" MODIFIED="1664701003318">
<node TEXT="Micro services" ID="ID_1812240485" CREATED="1664701010472" MODIFIED="1664701018023"/>
<node TEXT="DevOps" ID="ID_1893317474" CREATED="1664701018325" MODIFIED="1664701021342"/>
<node TEXT="Continuous delivery" ID="ID_1083276816" CREATED="1664701021789" MODIFIED="1664701028110"/>
<node TEXT="Containers" ID="ID_221077699" CREATED="1664701028453" MODIFIED="1664701032671"/>
</node>
<node TEXT="Tendances" ID="ID_1529270656" CREATED="1664701103261" MODIFIED="1664701106942">
<node TEXT="Poly Cloud" ID="ID_1703139387" CREATED="1664701114911" MODIFIED="1664701120431">
<node TEXT="on utilise des services diff&#xe9;rents&#xa;de CSP diff&#xe9;rents" ID="ID_40505204" CREATED="1664701120813" MODIFIED="1664701237519"/>
</node>
<node TEXT="Multi Cloud" ID="ID_978620372" CREATED="1664701120813" MODIFIED="1664701134078">
<node TEXT="M&#xea;me infra" ID="ID_1060505043" CREATED="1664701120813" MODIFIED="1664701191358"/>
<node TEXT="plusieurs CSPs" ID="ID_1086182602" CREATED="1664701191845" MODIFIED="1664701198414"/>
</node>
<node TEXT="IA" ID="ID_1444062388" CREATED="1664701245469" MODIFIED="1664701247358"/>
<node TEXT="Machine learning" ID="ID_1387664008" CREATED="1664701247773" MODIFIED="1664701251591"/>
<node TEXT="IoT" ID="ID_610318038" CREATED="1664701256157" MODIFIED="1664701258854"/>
<node TEXT="DevSecops" ID="ID_1061435043" CREATED="1664701261597" MODIFIED="1664701265407"/>
</node>
</node>
<node TEXT="Services overview" POSITION="right" ID="ID_1409747241" CREATED="1664700293455" MODIFIED="1664700574992">
<edge COLOR="#0000ff"/>
</node>
<node TEXT="" POSITION="left" ID="ID_1198026277" CREATED="1664700702502" MODIFIED="1664700702503">
<edge COLOR="#ff00ff"/>
</node>
</node>
</map>
