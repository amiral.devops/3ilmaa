# Docker

## Ressources

- <https://openclassrooms.com/fr/courses/2035766-optimisez-votre-deploiement-en-creant-des-conteneurs-avec-docker>

- [Docker](#docker)
  - [Ressources](#ressources)
  - [Génèse](#génèse)
    - [Architecture d'une machine virtuelle](#architecture-dune-machine-virtuelle)
    - [Les conteneurs](#les-conteneurs)
  - [Docker](#docker-1)
    - [Image \& Conteneur, quelle différence ?](#image--conteneur-quelle-différence-)
    - [Stateless \& Stateful](#stateless--stateful)
    - [Immutabilité](#immutabilité)
    - [Versions de Docker](#versions-de-docker)
    - [Installation sur opensuse Leap 15.2](#installation-sur-opensuse-leap-152)
  - [Commandes Docker](#commandes-docker)
    - [Exemple avec le serveur httpd](#exemple-avec-le-serveur-httpd)
    - [Donner un nom à un conteneur | docker run --name](#donner-un-nom-à-un-conteneur--docker-run---name)
    - [Suppression d'un conteneur | docker rm](#suppression-dun-conteneur--docker-rm)
    - [Entrer en interaction avec un conteneur | docker exec -ti](#entrer-en-interaction-avec-un-conteneur--docker-exec--ti)
    - [Afficher les logs d'un conteneur | docker logs](#afficher-les-logs-dun-conteneur--docker-logs)
    - [Afficher les détails d'un élément | docker inspect](#afficher-les-détails-dun-élément--docker-inspect)
  - [Les volumes](#les-volumes)
  - [Network \& expose ports](#network--expose-ports)
  - [Dockerfile](#dockerfile)
  - [Docker Compose](#docker-compose)
    - [Exemple docker-compose MySQL](#exemple-docker-compose-mysql)
    - [Exemple docker-compose Postgres](#exemple-docker-compose-postgres)


## Génèse

Historiquement, quand nous avions besoin de serveurs, nous achetions des serveurs physiques avec une quantité définie de CPU, de mémoire RAM ou de stockage sur le disque.

Or, on avait souvent besoin d'avoir de la puissance supplémentaire pour des périodes de forte charge (fête de Noël, par exemple). Ainsi, vous deviez acheter plus de serveurs pour répondre aux pics d'utilisation. 

Une fois les pics passés, les serveurs achetés ne servaient donc plus à rien. Une solution a alors été créée : la ***machine virtuelle***.

### Architecture d'une machine virtuelle

Cette solution permettait d'être plus économe qu'avec les serveurs physiques, mais cela reste encore lourd. En effet, il faut charger pour chaque VM, un OS, ses applications, dépendances etc...

Une VM de par sa lourdeur est plus lente à démarrer. Les ressources de la machine hôte (RAM, CPU, Réseau...) lui sont complètement allouées. Aussi, il faut noté que la VM est complètement isolé de l'hôte.

V![image-20220128133521147](img/image-20220128133521147.png)

Il était nécessaire de trouver quelque chose de plus léger : les **conteneurs**.

### Les conteneurs

Un conteneur Linux est un **processus** ou un ensemble de processus isolés du reste du système, tout en étant **légers**.

Le conteneur permet de faire de la ***virtualisation légère\***, c'est-à-dire qu'il ne virtualise pas les ressources, il ne crée qu'une **isolation des processus**. Le conteneur partage donc les ressources avec le système hôte.

![image-20220128134356251](img/image-20220128134356251.png)

Avantages des conteneurs :

- Ils ne réservent pas les ressources de la machine hôte (partage)
- Démarrage rapide, puisqu'il s'agit simplement d'une isolation des processus
- Environnement propice au développement, puisque le développeur peut travailler sur son propre conteneur, tout en étant proche de la cible, sur laquelle sera déployée l'appli.
- Aussi, sur une structure importante, les coûts d'infrastructure seront économisés.
- Etant très rapides à être démarrer, ils sont pratiques en cas de pic. Il est possible de déployer une salve de conteneurs pour absorber le surplus d'activité.

## Docker

Docker a été mis en place pour gérer la compatibilité entre des environnements métiers hérétogènes : business, développeur, testeur... et aussi techniques : linux, windows, mac...

*Aussi, il est important de noter que Docker ne peut exécuter que des images d'OS de type Linux !*

### Image & Conteneur, quelle différence ?

Un conteneur est une image chargée en mémoire et ayant un ou plusieurs processus système. Il est possible de charger plusieurs conteneurs à partir d'une seule image.

### Stateless & Stateful

- **Stateful** : un conteneur de base de données a besoin de sauvegarder un nouvel état à chaque transaction. Cela signifie que le conteneur devra être stateful.
- **Stateless** : or, un conteneur de services HTTP n'aura pas besoin de conserver un état, il sera donc stateless

### Immutabilité

Si un conteneur doit contenir une base de données, alors il devra y avoir un volume pour stocker la base de données.

### Versions de Docker

- Docker Community Edition (Linux seulement) ; -> Gratuite
- Docker Desktop (Mac ou Windows) ; -> Gratuite
- Docker Enterprise (Linux seulement).

### Installation sur opensuse Leap 15.2

1. Pour installer docker sur sa distribution Linux :

   ```
   sudo snap install docker
   ```

2. Ensuite, il faut créer un compte sur docker hub.

3. En ligne de commande exécuter la commande suivante :

    ```shell
    docker login
    Username: # renseigner ici le username
    Password: # renseigner le mot de pass
    ```

4. Pour s'assurer que Docker est bien installé, il faut exécuter la ligne de commande suivante :

   ```bash
   docker run hello-world
   ```

## Commandes Docker

```shell
# pour lister les images
docker images

# pour lister les conteneurs
docker ps
docker ps -a # all
docker ps -aq # all mais affiche uniquement leur ID

# pour récupérer une image docker
docker pull nom_image

# pour lancer un conteneur à partir d'une image
docker run nom_image

## pour lancer le conteneur en arrière plan
# -t : Allouer un pseudo TTY (terminal virtuel) au conteneur
# -i : Garder un STDIN ouvert (l'entrée standard plus précisément l'entrée clavier) au conteneur ; cela permet l'interaction
# -d : Exécuter le conteneur en arrière-plan et afficher l'ID du conteneur ; cela permet de ne pas voir les logs
docker run -tid nom_image

# pour arrêter un conteneur 
docker stop ID_CONTENEUR

# pour faire le nettoyage
    # l'ensemble des conteneurs Docker qui ne sont pas en status running ;
    # l'ensemble des réseaux créés par Docker qui ne sont pas utilisés par au moins un conteneur ;
    # l'ensemble des images Docker non utilisées ;
    # l'ensemble des caches utilisés pour la création d'images Docker.
docker system prune
docker container prune
docker image prune

## Afficher de l'aide
docker help
docker <sous-commande> --help

## Afficher des informations sur l'installation de Docker
docker --version
docker version
docker info

## Executer une image Docker
docker run hello-world

## Lister des images Docker
docker image ls
# ou
docker images

## Rechercher une image depuis le Docker hub Registry
# Afficher que les images officielles
docker search ubuntu --filter "is-official=true" 

## Télécharger une image depuis le Docker hub Registry
# prendra par défaut le tag latest
docker pull <IMAGE_NAME> 
# prendra le tag 16.04
docker pull ubuntu:16.04 
```

### Exemple avec le serveur httpd

```shell
# pour récupérer l'image
docker pull httpd

# pour lancer le conteneur 
# -d -> detached 
# it -> interactive avec un prompt ouvert sur le conteneur
# -p 8080:80 -> les requêttes sur 8080 -> 80
docker run -dit --name my-apache-app -p 8080:80 -v "$PWD":/usr/local/apache2/htdocs/ httpd:2.4
```

### Donner un nom à un conteneur | docker run --name

Pour la gestion des conteneurs, il est possible de leur attribuer un nom

```shell
docker run --name <CONTAINER NAME ou ID> -d -p 8080:80 httpd:latest
```

### Suppression d'un conteneur | docker rm

Aussi, avec avoir lancé un conteneur avec un nom, il ne sera pas possible de le relancer avec le même nom. Il faudra donc où le renommer ou alors le supprimer :

```shell
docker rm <CONTAINER NAME ou ID>

## Supprimer une image Docker
docker images rmi <IMAGE_ID ou IMAGE_NAME>  # si c'est le nom de l'image qui est spécifié alors il prendra par défaut le tag latest
    -f ou --force : forcer la suppression

## Supprimer tous les images Docker
docker rmi -f $(docker images -q)
```

### Entrer en interaction avec un conteneur | docker exec -ti

Après avoir lancé un conteneur, il est possible de rentrer en interaction avec un terminal bash

```shell
docker exec -ti <CONTAINER NAME ou ID> /bin/bash
```

### Afficher les logs d'un conteneur | docker logs

- **-f** : **suivre en permanence les logs du conteneur** (correspond à tail -f)
- **-t** : **afficher la date et l'heure de réception des logs d'un conteneur**

```shell
docker logs -ft <CONTAINER NAME ou ID>
```
### Afficher les détails d'un élément | docker inspect
```shell
docker inspect <CONTAINER NAME ou ID>
```

### Run test Maven

```bash
docker run 
--rm # supprime le container après l'éxécution
--name maven 
-v /var/lib/maven:/root/.m2 # docker va créer le répertoire local si inexistant
-v $(pwd):/usr/src/mymaven 
--network <network_name> 
-w /usr/src/mymaven # working directory
maven:3.3-jdk-8 # image docker à runner
mvn -B clean test # la commande à exécuter
```

### Run maven package

Cette commande construit le jar applicatif dans le dossier target.

```bash
docker run 
--rm # supprime le container après l'éxécution
--name maven 
-v /var/lib/maven:/root/.m2 
-v $(pwd):/usr/src/mymaven 
--network <network_name> 
-w /usr/src/mymaven # working directory
maven:3.3-jdk-8 # image docker à runner
mvn -B clean package # la commande à exécuter
```

## Les volumes

```shell
# pour créer un volume
docker volume create <VOLUME_NAME>

# pour lister les volumes
docker volume ls

# pour récupérer des informations sur un volume
docker inspect <ELEMENT_ID>
docker volume inspect <VOLUME_NAME>

# pour lancer un conteneur en lui assossiant un volume
docker run -d --name <CONTAINER_ALIAS> -v <VOL_NAME>:/<REP_IN_DOCKER_CONTAINER> <CONTAINER_NAME>

#sudo docker run -d --name yassine1 -v vol_yassine:/tmp nginx

docker run -d --name <CONTAINER_ALIAS> -v <LOCAL_REP_NAME>:/<REP_IN_DOCKER_CONTAINER> <CONTAINER_NAME>
```

## Network & expose ports


```shell
# lister les networks
docker network ls

docker network create my-net
docker run -itd --network=my-net busybox
```

## Dockerfile

Dans cette rubrique, nous allons voir comment créer une image Docker à l'aide du fichier *Dockerfile*. C'est l'équivalent du *package.json*  pour Node.js.

Le fichier se place à la racine d'un projet Java.

```Dockerfile
FROM openjdk:14-slim-buster
CMD mkdir /jar
COPY target/*.jar /jar/
EXPOSE 8080
```

Pour ce faire, le Dockerfile va décrire comment construire notre image par étape, communément appelée layer. L'enjeu est d'avoir le moins de layer possible afin de rendre le conteneur léger à l'exécution.

```shell
# pour créer une image à partir d'un fichier Dockerfile dans le répertoire courant
docker build -t <image-docker-name:1.0> .

# le -t pour décrire le tag de l'image
```

Une fois l'image construire, il est alors possible d'exécuter un container à partir de cette image ou de la pousser vers une registry.

### Run d'un container Spring Boot à partir d'une image

```bash
docker run -dti 
--name apitest # nom du container
--network learning-bash_generator_net 
-v $(pwd)/src/main/resources/application.properties:/etc/application.properties 
-p 8080:8080 
yassine:1.0 # nom de l'image qu'il faut instancier
java -jar jar/myapp1-0.0.1-XXX.jar # la commande à exécuter dans le container
--spring.config.location=file:/etc/application.properties
```

## Docker Compose

Docker Compose est un outil permettant la construction des conteneurs devenant complexes (multi layers, volumes etc...).

```yaml
version: 3.8
container_name:
depend_on: # service dépendant d'un autre
volumes:
networks:
environment:
healthcheck: # vérification de l'état de santé du container(retries, curl...)
restart: # modalité du restart du container
command: # commande passée au container
```

### Exemple docker-compose MySQL

```yaml
version: "3.8"
services:
  front:
    container_name: monnginx
    image: nginx
    ports:
      - 8080:80
    networks:
      http_net:
      bdd_net:
  bdd:
    container_name: bdd_box
    image: mysql
    command: --default-authentication-plugin=mysql_native_password
    environment:
      MYSQL_ROOT_PASSWORD: example
      MYSQL_DATABASE: tchoos
      MYSQL_USER: yassine
      MYSQL_PASSWORD: yassine_pwd
    networks:
      bdd_net:
    volumes:
      - bdd_vol:/var/lib/mysql/

networks:
  http_net:
  bdd_net:

volumes:
  bdd_vol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: /srv/bdd_db/

```

### Exemple docker-compose Postgres

```yaml
version: '3.7'
services:
  postgres:
    image: postgres:latest
    container_name: yassine-postgres
    environment:
    - POSTGRES_USER=yassine
    - POSTGRES_PASSWORD=yassine_pwd
    - POSTGRES_DB=mydb
    ports:
    - 5432:5432
    volumes:
      - postgres_data:/var/lib/postgres
    networks:
      - generator_net
volumes:
  postgres_data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: /home/yassine/Projects/learning-bash/postgres
networks:
  generator_net:
    driver: bridge
    ipam:
      config:
      - subnet: 192.168.56.0/24
```
