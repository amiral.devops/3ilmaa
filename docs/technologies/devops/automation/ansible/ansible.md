# Ansible

- [Ansible](#ansible)
  - [Ressources](#ressources)
  - [Concepts](#concepts)
  - [Inventory](#inventory)
  - [Exécution ping](#exécution-ping)
  - [Exécution d'un shell](#exécution-dun-shell)
  - [Ecrire dans un fichier distant](#ecrire-dans-un-fichier-distant)
  - [Les variables \& leur utilisation | \& hiérarchie](#les-variables--leur-utilisation---hiérarchie)
  - [group\_vars \& host\_vars](#group_vars--host_vars)
  - [Task](#task)
  - [Playbook \& son utilisation](#playbook--son-utilisation)
  - [Copy d'un fichier](#copy-dun-fichier)
  - [Templates](#templates)
  - [Installation de nginx \& mise à jour nginx](#installation-de-nginx--mise-à-jour-nginx)
  - [Ansible Galaxy](#ansible-galaxy)
  - [Requirements](#requirements)
  - [Module lineinfile](#module-lineinfile)
  - [Intégration de Ansible dans Jenkins](#intégration-de-ansible-dans-jenkins)


## Ressources

## Concepts

- Inventory : descriptif de l'ensemble des serveurs à administrer
- Module : est une fonction qui agit sur le serveur. Cf. galaxy hub pour la liste des fonctions disponibles. Exemple: installer docker
- Tâche : une tâche appelle un module sur un ensemble de serveurs
- Rôle : regroupement de tâches. Il est idéal d'être le plus fin possible dans sa définition
- Playbook : définition de l'ensemble de rôles devant être éxécutés dans le but d'avoir un ensemble cohérent et fonctionnel. Lien entre invesntory, role & taches
- Groupes : divisions de serveurs
- Variables : variables utilisables dans la conf ansible.

## Inventory

Répertorie la liste des serveurs

```yaml
all:
  children:
    jenkins:
      hosts:
        192.168.56.2:
    registry:
      hosts:
        192.168.56.5:
```

## Exécution ping

```shell
# -k ouvre le prompt au password
# -m module

ansible -i inventory.yml all -u vagrant -k -m ping
```

## Exécution d'un shell

```shell
ansible -i inventory.yml all -u vagrant -k -m shell -a "hostname"
```

## Ecrire dans un fichier distant

```shell
ansible -i inventory.yml all -u vagrant -m shell -a "echo 'Yassine' >/tmp/test"
```

## Les variables & leur utilisation | & hiérarchie

inventory C group_vars C host_vars


```yaml
all:
  vars:
    nom: "Salut Yassine"
  children:
    jenkins:
      hosts:
        192.168.56.2:
    registry:
      hosts:
        192.168.56.5:
```

```shell
ansible -i inventory.yml all -u vagrant -m shell -a "echo '{{nom}}' >/tmp/test"
```

## group_vars & host_vars

il faut crée ces deux répertoires et renseigner des fichiers yaml comme suit :

```shell
yassine@localhost:~/Projects/ansible> tree
.
├── group_vars
│   ├── all.yml
│   └── registry.yml
├── host_vars
│   ├── 192.168.56.2.yml
│   └── 192.168.56.5.yml
├── inventory.yml
```

## Task

```yaml
- name: task 1
  shell: echo "Rebonjour Yassine from role & playbook !!!!" > /tmp/yadine

```

## Playbook & son utilisation

C'est un objet qui permet de manipuler à la fois les items suivants :
- inventory
- roles
- tâches

Il faut d'abord crée un répertoire pour contenir le role et exécuter la commande suivante :

```shell
mkdir roles
cd roles
ansible-galaxy init <ROLE_NAME>

# cela va créer la structure de fichiers suivante

yassine@localhost:~/Projects/ansible> tree
.
├── ...
└── roles
    └── monroles
        ├── defaults
        │   └── main.yml # variables par defaut du role
        ├── files # pour déployer un fichier statique sur un cluster
        ├── handlers # trigger qui sera appelé -> restart service
        │   └── main.yml
        ├── meta # metadatas, genre de hub
        │   └── main.yml
        ├── README.md
        ├── tasks # point d'entrée du role, ce fichier sera lu en premier
        │   └── main.yml
        ├── templates # fichier modèle de variables
        ├── tests # pour tester le role
        │   ├── inventory
        │   └── test.yml
        └── vars # renseigner les variables spécifiques du roles
            └── main.yml


# exécution du playbook
ansible-playbook -i inventory.yml -u vagrant playbook.yml
```

## Copy d'un fichier

```yaml
- name: task 1
  copy:
    src: monfichier.txt # fichier source ; il est aussi possible de renseigner un fichier à la racine du projet
    dest: /tmp/amiral.txt  # le fichier de destination
    owner: vagrant
    mode: 0755
```

## Templates

Permet de manipuler des fichiers avec paramétres (host_vars, group_vars)

```yaml
- name: task 1
  template:
    src: monfichier.txt.j2 # utilisation de jinja templating
    dest: /tmp/amiral.txt  # le fichier de destination
    owner: vagrant
    mode: 0755
```

## Installation de nginx & mise à jour nginx

```yaml
- name: installation de nginx 
  apt:
    name: nginx
    state: present
    update_cache: yes
    cache_valid_time: 3600
```

```yaml
# fichier task/main.yml
- name: installation de nginx 
  apt:
    name: nginx
    state: present
    update_cache: yes
    cache_valid_time: 3600

- name: édition du vhost par défaut
  template:
    src: default.j2
    dest: /etc/nginx/sites-available/default
  notify: nginx_reload

# fichier handlers/main.yml
# handlers file for monroles
- name: nginx_reload
  service:
    name: nginx
    state: reloaded

```
## Ansible Galaxy

C'est un hub communautaire où les devs partagent leur roles prêts à l'emploi

-> https://galaxy.ansible.com/ui/

## Requirements

Permet d'installer des roles prêts à l'emploi via le hub galaxy par exemple

```yaml
# fichier requirements.yml à la racine du projet

- src:  https://github.com/ultransible/docker.git 
  name: docker
  version: master
  scm: git

# exécution du fichier
ansible-galaxy install --roles-path roles/ -r requirements.yml
```

On peut ensuite modifier le playbook pour indiquer le role installé via le requirement.

```yaml
- name: playbook 1
  hosts: dev 
  become: yes
  roles:
  - docker

# exécution avec la même commande
ansible-playbook -i inventory.yml -u vagrant playbook.yml
```

## Module lineinfile

Permet de modifier/ajouter/supprimer une ligne dans un fichier.

## Intégration de Ansible dans Jenkins

Tout d'abors, il faut activer le plugin Ansible dans Jenkins.
Ensuite, mettre à jour le jenkinsFile avec la section suivante pour déclencher l'action Ansible pour provisionner la VM cible.

```groovy
stage('ANSIBLE - Deploy'){
    git branch: 'master', url: 'http://gitlab.example.com/pipeline/deploy-ansible.git'
    sh "mkdir -p roles"
    sh "ansible-galaxy install --roles-path roles -r requirements.yml"
    ansiblePlaybook (
        colorized: true,
        playbook: "install-myapp.yml",
        hostKeyChecking: false,
        inventory: "env/${branchName}/hosts",
        extras: "-u vagrant -e 'image=$imageName:${version}-${commitId}' -e 'version=${version}'"
        )
}
```

Jenkins va se connecter à la vm cible par ssh. Il faut donc créer une paire de clefs sur le serveur Jenkins et faire un copy vers la machine cible.

```shell
# se placer en tant qu'user jenkins
sudo su - jenkins

# générer une paire de clef
ssh-keygen

# copier la clef publique fraichement générée dans le fichier /home/vagrant/.ssh/authorized_keys
ssh-copy-id -i .ssh/id_rsa.pub vagrant@ip_machine_cible

```