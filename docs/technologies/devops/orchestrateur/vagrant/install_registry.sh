#!/bin/bash

## install registry

IP=$(hostname -I | awk '{print $2}')

echo "START - install registry - "$IP

echo "[1]: update and install utils"
apt-get update -qq >/dev/null
apt-get install -qq -y git wget curl apache2-utils

echo "[2]: install docker and docker-compose"
curl -fsSL https://get.docker.com | sh; 
curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo "[3]: prepare directories"
mkdir ~/docker-registry
mkdir ~/docker-registry/data
mkdir ~/docker-registry/auth
cd ~/docker-registry/auth
htpasswd -Bbc registry.password yassine yassine_pwd

echo "[4]: install registry"
echo "[5]: prepare docker-compose.yml file"
cd ~/docker-registry
echo "
version: '3'

services:
  registry:
    restart: always
    image: registry:2.8.3
    ports:
    - "5000:5000"
    environment:
      REGISTRY_AUTH: htpasswd
      REGISTRY_AUTH_HTPASSWD_REALM: Registry
      REGISTRY_AUTH_HTPASSWD_PATH: /auth/registry.password
      REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY: /data
    volumes:
      - ./auth:/auth
      - ./data:/data
" > docker-compose-registry.yml

docker-compose -f docker-compose-registry.yml up -d

echo "END - install registry"

