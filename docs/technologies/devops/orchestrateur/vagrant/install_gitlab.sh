#!/bin/bash

IP=$(hostname -I | awk '{print $2}')

echo "START - install gitlab - "$IP

echo "[1]: install dependencies"
apt-get update -qq >/dev/null
apt-get install -qq -y vim git wget curl openssh-server ca-certificates perl

echo "[2]: install gitlab"
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee

echo "END - install gitlab"
