# AWS - Services overview

## Introduction

### Le marché

![image-20221004215130584](img/image-20221004215130584.png)

### Pourquoi AWS

- Beaucoup plus de fonctionnalités
- plus secure
- innovation plus forte (lambda)
- service le plus mature
- surface plus importante en terme de régions
  - 1 régions = plusieurs datacenter en cluster

## AWS Compute services - EC2

### Elastic Compute Capacity (EC2)

- service scalable
- on peut en lancer plusieurs à la fois, n'importe où
- sécurité & réseau gérable
- espce de stockage accessible

### Fonctionnalités EC2

- Amazon Machine Image
  - Amazon propose des images prêtes
  - il en existe aussi des communautaires
  - et il est possible de créer des customs (masters)
- Instance types
  - Choix du CPU
  - RAM
- Key pairs
  - gestionnaire de clef privée/publique
- Instance store volumes
  - espace de stockage <u>temporaire</u> attaché à l'EC2
  - les données sont supprimées à la fin de l'instance
  - utile pour les caches, buffer etc...
- Amazon EBS volumes
  - Elastic Block Store
  - ici le stockage est permanent et est indépendant de l'instance
- Regions & available zones
  - on peut gérer quelle zone accède à quel service/EC2 etc...
- Security groups
  - ports, protocoles
  - gestion du traffic
  - gestion des roles
- Elastic IP addresses
  - permet d'attacher de manière fixe une @IP à une instance EC2
- Tags
  - permet de tagger les instances et ressources afin de les repérer plus rapidement
- Virtual Private Cloud - VPC
  - permet de déterminer une zone privére (d'entreprise, organisation) vont être déployée les instances EC2

### EC2 pricing modèles

#### On demand instances

![image-20221004221752993](img/image-20221004221752993.png)

#### Spot instances

![image-20221004221631111](img/image-20221004221631111.png)

#### Instances réservées

![image-20221004221718817](img/image-20221004221718817.png)

### Elastic Block Store



### Créer une instance EC2

A la fin du process, il est possible de récupérer la clef privée pour accéder à l'instance ec2 au format pem.

```bash
#!/bin/bash
yum update -y
yum install -y httpd
systemctl start httpd
systemctl enable httpd
```

Une fois téléchargée, il faut changer les droits sur la clef privée :

```sh
# changer les droits
sudo chmod 400 file.pem

# accéder à l'instance ec2 | ec2-user est l'utilisateur technique pour l'ec2
ssh -v -i <key_file>.pem ec2-user@13.38.62.147

# avoir les accès root
sudo su -
```

A ce stade, le terminal ouvre une sessions sur l'instance ec2

### AWS Elastik Beanstalk

### AWS Lambda
C'est un service asynnchrone qui permet de gérer les évènements tels que l'envoi d'une photo, une date d'anniversaire, un changement de position GPS .... et de déclencher une action suite à cet évènement.

Aussi, le service est auto-géré. cela signifie que AWS gère la montée/baisse en charge du service si nécessaire.

Le service fonctionne avec un bucket S3 et DynamoDB.

Le service permet de charger un code personnel, d'utiliser une lambda prête à l'usage ou d'appeler une simple fonction comme la compression d'un fichier, la conversion d'une image etc..

Le service est de type pay per use.

## AWS Storage services

### Introduction to S3

La problématique : les données d'une application et son backup nécessite énormément de capacité de stockage. Aussi, mettre tout ce système de stockage (data center, RAID, réplication etc...) est très complexe. Enfin, il est très difficile de déterminer l'espace de stockage nécessaire d'ici n années.

AWS a mis au point S3 Simple Storage Service pour traiter cette problématique. Le service est disponible 99.999999999% du temps.

#### Fonctionnalités S3

![image-20221010225641813](img/image-20221010225641813.png)

#### Cas d'usage

![image-20221010225831729](img/image-20221010225831729.png)

### Amazon S3 Storage Classes

#### Les classes disponibles

![image-20221010230652690](img/image-20221010230652690.png)

#### S3 Standard

Pour un usage fréquent et pour une donnée accessible immédiatement.

![image-20221010231002929](img/image-20221010231002929.png)

#### S3 Standard - IA

Utile pour les backup ou recovery, L'accès est donc moins accès sur l'immidiateté mais doit être disponible.

![image-20221010231201200](img/image-20221010231201200.png)

#### S3 Intelligent - Tiering

C'est un service qui se charge de stocker les datas selon leur accesibilité sur le S3 Standard ou S3 Stabdard - IA. Utile lorsqu'il est difficile de décider entre les deux premiers services.

![image-20221010231412084](img/image-20221010231412084.png)

#### S3 One Zone - IA

Ici ce sont l'un ou les premiers services qui sont répliqués sur tout une zone.

Utile pour un accès moins immédiat, pour des datas non critiques et pour une donnée facilement reproductible.

![image-20221010231642406](img/image-20221010231642406.png)

#### S3 Glacier

Pour les données durables, secure low cost. Le service permet de récupérer les données entre quelques minutes et quelques heures selon la tarification. 

![image-20221010231725456](img/image-20221010231725456.png)

#### S3 Glacier Deep Archive

Utile pour les accès long temes (5/10 ans) et nécessitant un accès immédiat comme pour la finance, assurance etc...

![image-20221010231853162](img/image-20221010231853162.png)

### Création d'un Bucket S3

### Amazon S3 Lyfecycle rules

C'est un service qui permet de gérer la suppression, stockage, backup du contenu des différents espace de stockage.

### Introduction to Amazon EFS (Elastic File System)

C'est un système de partage distribué de fichiers utilisant le protocole NFSv4. Le service est accessible depuis un VPC. Le service est auto-scale up & down selon les capacité de stockage. Le pay per use en fonction de la capacité de stockage.

Le service permet le contrôle depuis le filtre IP, mais aussi depuis un LDAP avec users & groups, AWS IAM etc...

![image-20221021211504615](img/image-20221021211504615.png)

### Introduction to Amazon FSx




## AWS Networking services

### AWS VPC
C'est un réseau virtuel privé avec sous-réseaux, gateways, routers, plage d'IP v4, v6.

Les composants :

- Subnet
- Route table
- Internet gateway pour sortir vers Internet
- NAT gateway
- VPC endpoints pour se connecter à différents services AWS
- VPC Peering pour communiquer entre 2 VPCs

### AWS CloudFront (à approfondir)

C'est un service qui permet de délivrer du contenu static et/ou dynamique.

### AWS Route53 (à approfondir)

C'est un service DNS qui permet de récupérer les @IP des différents endpoints fournissant des services AWS.

## AWS Database services

### Amazon RDS (Relational)

C'est un service AWS qui permet des bases de données relationnelles de type : MySQL, PostgreSQL, Oracle, MariaDB, SQLServer, <u>Amazon Aurora</u> et qui permet de s'affranchir des contraintes suivantes :

![image-20221021224124143](img/image-20221021224124143.png)

- facile à administrer
- acalable
- disponibilité et durabilité + recovery
- sécurisé
- rapide

### Amazon DynamoDB

C'est un service intégré AWS où le service rendu est plus haut de gamme

### Amazon Redshift (à approfondir)

C'est un service qui permet de faire des graphiques et autres KPI sur les données


## AWS Developer tools

### AWS Cloud9 (IDE)

C'est un IDE qui permet d'écrire du code et de le buider. L'avantage est qu'il est déjà mapper avec les services AWS (s3, network, DB etc...)

### AWS CodeCommit (Git)

C'est un gestionnaire de version intégré AWS.

### AWS CodeBuild (Continuous Integration)

C'est un jenkins intégré qui fait de l'intégration continue au fur et à mesure que les devs poussents leur code. Le service permet d'intégrer des tests automatisés.

### AWS CodeDeploy (Continuous Delivery)

C'est un service qui permet de déployer des artefacts de build vers des environnement (test, intégration, prod).

### AWS CodePipeline !!!

C'est un service qui permet d'automatiser le processus env test -> env int -> env prod manuellement. Le service permet de faire des rapports sur les succès et échecs. 

Il est possible avec ce service de délivrer des mises à jour automatiques.

## AWS Monitoring & Auditing



## AWS Identity services

### AWS IAM

Identity & Access Management. C'est un service qui permet de :

- créer & gérer des utilisateurs & des groupes
- gérer les autorisations
- gérer les access
- authentification multi facteur
- analyse des access
- intégrer le service avec le corporate

#### IAM identités
- User -> avec un compte IAM
- Groupes -> ajouter un compte dans un groupe
- Roles -> définissent les access

### AWS Cognito

User identity service et DB synchronisation

### AWS SSO


## AWS : other services



## AWS : les  bonnes pratiques

