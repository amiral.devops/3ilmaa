# BASH

## Récupérer les arguments

```bash
$1
$2
$# # nombres d'arguments
$* # tous les args en une variable
$@ # tous les args en un tableau
$0 # pour avoir le nom du script courant
``` 

Cf. learning-bash project
