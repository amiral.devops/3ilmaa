# PoC : 'Hello World !' using Blockchain

## Prerequities

- Installing GO

  ```shell
  # We get the GO package
  wget https://go.dev/dl/go1.17.5.linux-amd64.tar.gz
  
  # Then, we install it on /usr/local/go
  sudo tar -C /usr/local -xzf go1.17.5.linux-amd64.tar.gz
  
  ```

- Set GO environment variables

  ```bash
  # Add in your ~/.profile following lines
  export GOPATH=/home/yassine/go
  export GOHOME=/usr/local/go
  export PATH=$PATH:$GOPATH:$GOHOME/bin
  ```

  

- Then, you have to source tour .profile

  ```bash
   cd && . .profile
  ```

- Let's check if GO is OK !

  ```bash
  # In console, print the GO's version 
  go version
  ```

## Starport project

### What does Starport do

- Installation

```bash
# We get the Starport
curl https://get.starport.network/starport | bash

# Then, we'll move Starport executable to /usr/local/bin
sudo mv starport /usr/local/bin/
```

- Let's check if Starport is OK !

```bash
# In console, print the Starport's version
starport version
```

## Create a simple custom blockchain

```bash
starport scaffold chain github.com/cosmonaut/infosys

# Enter in the new project, freshly created
cd infosys

# Start the blockchain
starport chain serve

# If something goes wrong, reset the blockchain
starport chain serve --reset-once

# Start the web-app using cosmos blockchain
cd vue
npm install
npm run serve

# Remove all Starport binairies
sudo rm $(which starport)
```

- At this point, take care of details printed on the console, like private address using a mnemonic format.

## rest to discover

- architecture
  - functional
  - flows
  - API / connectivity
- tendermint
- starport
- faucet
- IBC
- vechain 
  - sync -> browser of VeChain apps
  - connex -> JS lib to substitute to Ethereum web3.js
  - thor

## VeChain

```bash
mkdir playgrounds
cd playgrounds
git clone https://github.com/vechain/thor-sync.electron.git
# install dependencies
npm install

# build electron application for production
npm run build
```

## Tips

```bash
# install python 2
# install build-essential KO
# remove package-lock.json file
```

