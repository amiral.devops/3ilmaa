# Lesson 1

## Pragma declaration

We start by declare which compiler version have to be used to compile code.

```
pragma solidity ^0.4.19;
```

## Contract declaration 

Then we declare our contract :

```
Contract ZombieFactory {
	// implement code here
}
```

## Unsigned integers declaration

We can declare unsigned integer like this :

```
uint dnaDigits = 16;
```

## Math power declaration

```
int dnaDigits = 16;
math power
uint dnaModulus = 10 ** dnaDigits;
```

## Data Struct declaration

For structured datas, we can use Struct :

```
struct Zombie{
	string name;
	uint dna;
}
```

## Public Array object declaration

```
Zombie[] public zombies;
```

## Function declaration & params naming convention

Parameters which are transmitted to functions are named with an "_" just before

```
function createZombie(string _name, uint _dna) {} 
```

## Create an object & add it to an array

We can use *push* method of the array object, just declared above

```
function createZombie(string _name, uint _dna){
	// add a zombie to the zombies array
	zombies.push(Zombie(_name, _dna));
}
```

## Private function declaration & naming convention

To make a function private, convention recommends to put "_" before the function name. And finally, we add *private* key word to the end of the function signature.

```
function _createZombie(string _name, uint _dna) private {
	// add a zombie to the zombies array
    zombies.push(Zombie(_name, _dna));
}
```

## Return & view declaration

If a function have to return an something, we specified the returned value at the end of the signature. Also, if the function does not change nothing, then we can declare it as a view :

```
function _generateRandomDna(string _str) private view returns (uint){}
```

## Hash sha256 function usage

```
function _generateRandomDna(string _str) private view returns (uint){
	// hash a string to build a uniq ID
	// format hash to uint
	uint rand = uint(keccak256(_str));
	return rand % dnaModulus;
}
```

## Public function declaration

To make a function public, as we did a private function, we add *public* key word instead *private* key word.

```
function createRandomZombie(string _name) public {
	uint randDna = _generateRandomDna(_name);
	createZombie(_name, randDna);
}
```

## Event & trigger declaration

The event object is declared as a variable.

```
event NewZombie(uint zombieId, string name, uint dna);
```

Then, we can trigger it during function execution : 

```
function _createZombie(string _name, uint _dna) private {
	// add a zombie to the zombies array
	zombies.push(Zombie(_name, _dna));}
	uint id = zombies.push(Zombie(_name, _dna)) - 1;

	// event triggered
	NewZombie(id, _name, _dna);
}
```

## Mapping declaration

Mapping works like a hash (key-value)

```
mapping (uint => address) public zombieToOwner;
mapping (address => uint) ownerZombieCount;
```

