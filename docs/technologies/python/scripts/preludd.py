# -*- coding: utf8 -*-

print("---------------------- 1st test -> STRING ----------------------------");

input_value = input("1- Veuillez saisir un entier \n");
print ('1- La valeur saisie est : {} et son type est {}'.format(input_value, type(input_value)));

print(type(input_value));

if input_value.isdigit():
	print("c'est un entier");
else:
	print("ce n'est pas un entier");

print("---------------------- 2nd test -> INT ----------------------------");

other_value = int(input("2- Encore une autre saisie svp \n"));
print ('2- La valeur saisie est : {}'.format(other_value));
print (type(other_value));

print("---------------------- 3th test -> FLOAT ----------------------------");
float_value = float(input("3- Une toute dernière saisie svp \n"));
print("3- La valeur saisie est : {}".format(float_value));
print(type(float_value));
