## Ressources

- Compilateur en ligne -> https://www.codabrainy.com/python-compiler/

## Compilateur

```python
num_wallet = "2"
other_num = "4"
print(num_wallet + other_num) # -> 24

my_int1 = 1
my_int2 = 2
print(my_int1 + my_int2) # -> 3

hop = "1"
hip = 2
print(hip + hop) # -> TypeError: unsupported operand type(s) for +: 'int' and 'str'
```

## Types d'objets

### Primitifs

Integers & floats

```python
>>> my_integer = 1
>>> my_float = 1.3 # '.' pour les décimaux !!
>>> print (my_integer + my_float) # -> 2.3
```

### Liste / tableau [,,,,]

```python
characters = ["Alvin et les Chipmunks", "Babar", "Betty Boop", "Calimero", "Casper", "Le chat potté", "Kirikou"]

print(characters[1]) # -> Babar

#--------------------------------------------------------
# il est possible de mettre plusieurs types dans la liste
characters = ["Alvin et les Chipmunks", 3.6, "Betty Boop", "Calimero", "Casper", "Le chat potté", "Kirikou"]

print(characters[1]) # -> 3.6

#--------------------------------------------------------
quote = "Ecoutez-moi, Monsieur Shakespeare, nous avons beau être ou ne pas être, nous sommes !"
quote_2 = "On doit pouvoir choisir entre s'écouter parler et se faire entendre."
all_quotes = [quote, quote_2]

#--------------------------------------------------------
quote_2 = "nouvelle chaine de caractère"
print(all_quotes) # -> ['Ecoutez-moi, Monsieur Shakespeare, nous avons beau être ou ne pas être, nous sommes !', "On doit pouvoir choisir entre s'écouter parler et se faire entendre."]
```

### Tuples (,,,,)

C'est une structure de données ...TBD

```python
paris = (48.856578, 2.351828)
```

### Dictionnaire {'' : ''}

```python
english_french_dict = {"un": "one",
                       "deux": "two",
                       "trois": "three"
                      }
print(english_french_dict['deux']) # -> 'two'

```

### Type request

Pour connaitre le type de l'objet manipulé.

```python
>>> type("hello!") <class 'str'>
>>> type(34) <class 'int'>
>>> type(1.3) <class 'float'>
>>> type({"key": "value"} <class 'dict'>
>>> type(["salut"]) <class 'list'>
```

## Scripting

En première ligne du script, permet de charger le programme en UTF-8.

```python
# -*- coding: utf8 -*-
```

### Saisie clavier

Il est possible de donner la main à l'utilisateur, depuis un script, pour saisir une valeur en entrée du clavier.

```python
# -*- coding: utf8 -*-

print("---------------------- 1st test -> STRING ----------------------------");

input_value = input("1- Veuillez saisir un entier \n");
print ('1- La valeur saisie est : {} et son type est {}'.format(input_value, type(input_value)));

print(type(input_value));

if input_value.isdigit():
	print("c'est un entier");
else:
	print("ce n'est pas un entier");

print("---------------------- 2nd test -> INT ----------------------------");

other_value = int(input("2- Encore une autre saisie svp \n"));
print ('2- La valeur saisie est : {}'.format(other_value));
print (type(other_value));

print("---------------------- 3th test -> FLOAT ----------------------------");
float_value = float(input("3- Une toute dernière saisie svp \n"));
print("3- La valeur saisie est : {}".format(float_value));
print(type(float_value));
```



## Opérateurs

### If | elseif | else

Il est important de respecter l'indentation dans les if-else. Aussi, il ne faut surtout pas indenter quand il n'y en a pas besoin.

```python
if user_answer == "B":
    pass # passe à la suite
elif user_answer == "C":
    print("C pas la bonne réponse ! Et G pas d’humour, je C...")
else:
    # show another quote
```

## Les fonctions

### déclaration d'une fonction

```python
def get_random_item_in(my_list):
    # TODO: get a random number
    item = my_list[0] # get a quote from a list
    print(item) # show the quote in the interpreter
    return "program is over" # returned value
```



### appel d'une fonction
```python
# appel d'une fonction
ret = get_random_item_in(quotes);
if ret is None :
    print("retour null");
else :
    print("retour non null : {}".format(ret));
```

## Boucles

### While

```python
while var != val :
	# do something
```

### For

```python
for item in a_list :
	# do something
```

## Functions python sur les objets

### String

```python
“hello world!”.split()
['hello', 'world!']

“       hello world!      “.strip()
“hello world!”

"hello world!".capitalize()
"Hello world!"

"hello world!".upper()
"HELLO WORLD!"

"HELLO WORLD".lower()
"hello world!"

print ('2- La valeur saisie est : {}'.format(other_value));
```

### Nombre

```python
(2.5).is_integer()
False
```



### Liste

```python
# permet de retrouver l'index de la valeur cherchée
ma_liste.index("Babar") 

# ajouter un élément à la liste
ma_liste.append("Mowgli")

# ajouter un élément à un certain index
ma_liste.insert(4, "Balou")

# modifier un élément à un certain index
ma_liste[1] = "la valeur remplacante"

# supprimer le dernier élément de la liste
ma_liste.pop()

# supprimer un élément indéxé de la liste
ma_liste.pop(4)

# supprimé le premier élément défini de la liste
ma_liste.remove("elmnt_defini")

# connaitre la longueur d'une liste
len(ma_liste)

# accéder au dernier élément de la liste
ma_liste[-1]
```

### Dictionnaire

```python
>>> program = {"quotes": ["Ecoutez-moi, Monsieur Shakespeare, nous avons beau être ou ne pas être, nous sommes !", "On doit pouvoir choisir entre s'écouter parler et se faire entendre."], "characters": ["alvin et les Chipmunks", "Babar", "betty boop", "balimero", "casper", "le chat potté", "Kirikou"]}
>>> program["characters"]
[“alvin et les Chipmunks”, “Babar”, “betty boop”, “balimero”, “casper”, “le chat potté”, “Kirikou”]

print(program["characters"][0]) # stdout -> "alvin et les Chipmunks"
program["characters"] = "Un nouveau nom"
print(program["characters"][0]) # stdout -> "Un nouveau nom"

# mettre à jour des valeurs définies
>>> program.update({"characters" : ["Alvin", "Père Noël"], "quotes": ["Une citation unique qui sera sauvegardée"]})

# supprimer une valeur définie
program.pop("quotes")
```

## Import module

```python
import module_name

import turtle
turtle.forward(100)

from turtle import *
color('red', 'yellow')
begin_fill()
while True:
    forward(200)
    left(170)
    if abs(pos()) < 1:
        break
end_fill()
done()
```

