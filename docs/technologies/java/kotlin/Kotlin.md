# Kotlin

## Ressources

- https://openclassrooms.com/fr/courses/5353106-initiez-vous-a-kotlin
- https://play.kotlinlang.org/byExample/overview

## Généralités

### Déclaration de variables

```kotlin
// 1
val question = "What's your name?"

// 2 
val question: String = "What's your name?"

// 3 
var question: String = "What's your name?"
```

### Compilation

Le compilateur de Kotlin possède une fonctionnalité appelée **"l’inférence de type"**, ou "type inference", en anglais. Pour que cela soit plus parlant, comprenez par "inférence" la notion de **"déduction"** !

Kotlin se voulant être un langage très intelligent, ses développeurs ont souhaité lui offrir les avantages de lisibilité des langages "dynamiquement typés", mais sans leurs inconvénients. En effet, pour Kotlin, ce sera au moment de la **compilation** (et non au moment de l’exécution) que les types de vos variables seront **automatiquement déduits**.

```kotlin
val name: String = "Phil"
val age: Int = 27
val isDeveloper: Boolean = true

// est égal à ceci :

val name = "Phil"
val age = 27
val isDeveloper = true
```

### Null Safety

En Kotlin, il est possible de spécifier précisément si une variable peut être null ou pas. On peut ainsi le spécifier en rajoutant '?' après le type.

```kotlin
var message: String? = "My message can possibly be null !"
message?.toUpperCaset() // la méthode ne sera pas exécuté si l'objet est null

//Code OK
var message: String? = "I’m learning Kotlin!"
message = null
println(message.toString())

// Code KO
var message1: String = "I’m learning Kotlin!"
message1 = null
println(message1.toString())
    
```

### Les constantes

```kotlin
// Java
public static final String SERVER_URL = "https://my.api.com";

// Avec Kotlin, le mot clef 'static' n'existe plus -> 'const'
const val SERVER_URL: String = "https://my.api.com"
```

### Initialisation différée

Il est possible de déclarer une variable, mais de l'initialiser plus tard. Pour celà, il faut le déclarer explicitement

```kotlin
// Le code suivant ne fonctionnera pas, 'Property must be initialized or be abstract'
var username: String
var username: String? = null

// La solution
lateinit var username
```

### Les méthodes / déclaration

- Une expression renvoie un retour
- Une instruction ne renvoie pas de retour
- Kotlin considère les structures de contrôles comme des expressions

Pattern de déclaration d'une fonction :

- fun funcName(param: Type): Retour = expression
- fun funcName(param: Type): Retour {corps de la méthode}

```kotlin
fun main(args: Array<String>) {
    println("Hello, world!")
}

// Ici le retour est de type Int
private fun minOf(a: Int, b: Int): Int {
    return if (a < b) a else b
}

// Autre exemple
fun main(args: Array<String>) {
    println("Hello, world!")
}
var hello: Unit = main(arrayOf("")) // Unit -> type void
```

### Excercice corrigé

```kotlin

// Corrigez cette fonction
fun sayMyName(name: String) = println(" $name ") 

// Simplifiez cette fonction
/*fun sayHello(): String {
    return "Hello"
}*/
fun sayHello() = "Hello"

// Simplifiez cette fonction
/*fun isStudent(isLearning: Boolean): Boolean {
    if (isLearning){
        return true
    } else { 
        return false    
    }
} */
fun isStudent(isLearning: Boolean): Boolean { return isLearning}

fun main(args: Array<String>) {
    println( sayHello() )
    sayMyName("Yassine") 
    println( "Is Student : ${isStudent(false)} " ) 
}

```

### Visibilité des variables

En Kotlin, il y a quatre niveaux différents :

- `private` : Un membre déclaré comme  `private` sera visible uniquement dans la classe où il est déclaré.
- `protected` : Un membre déclaré comme  `protected` sera visible uniquement dans la classe où il est déclaré ET dans ses sous-classes (via l’héritage).
- `internal` : Un membre déclaré comme  `internal` sera visible par tous ceux du même module. Un module est un ensemble de fichiers compilés ensemble (comme une librairie Gradle ou Maven, par exemple).
- `public` : Un membre déclaré comme  `public` sera visible partout et par tout le monde.

## Les classes

### Déclaration

- Lors des déclarations, Kotlin met une visibilité publique par défaut
- Pour les classes, Kotlin n'a pas besoin de déclarer :
  -  les propriétés 
  - ni les assesseurs (getter) 
  - ni les mutateurs (setter). 
  - le constructeur par défaut, aussi appelé constructeur primaire

```kotlin
class User(var email: String, var password: String, var age: Int)
```

Est équivalent en Java à ceci :

```java
public class User {
    
    // PROPERTIES
    private String email;
    private String password;
    private int age;
    
    // CONSTRUCTOR
    public User(String email, String password, int age){
        this.email = email;
        this.password = password;
        this.age = age;
    }
    
    // GETTERS
    public String getEmail() { return email; }
    public String getPassword() { return password; }
    public int getAge() { return age; }
    
    // SETTERS
    public void setEmail(String email) { this.email = email; } 
    public void setPassword(String password) { this.password = password; }
    public void setAge(int age) { this.age = age; }
    
}
```

### Instanciation

Kotlin recommande de rendre les objets immaubles avec le mot clef 'val' même si leur propriétés resteront modifiables.

Aussi, il n'est plus utile de mettre le mot clef 'new' pour instancier un objet

```kotlin
val user = User("hello@gmail.com", "azerty", 27)
```

### Assesseurs (getter) & mutateurs (setter)

#### Ressources

- https://kotlinlang.org/docs/properties.html#backing-fields

Avec Kotlin, ils sont très différents de Java.

```kotlin
// Avec ce constructeur, toutes les propriétés sont accessibles & modifiables
val user = User("hello@gmail.com", "azerty", 27)
user.email // Getter
user.email = "new_email@gmail.com" // Setter

// Ici, il n'y aura pas de setter sur l'email
class User(val email: String, var password: String, var age: Int)

// Pour modifier un accesseur ou un mutateur; on va enlever le mot clef var avant email
class User(email: String, var password: String, var age: Int){
    var email: String = email
        get() { 
            println("User is getting his email."); 
            return field 
        }
        set(value) { 
            println("User is setting his email"); 
            field = value 
        }
}

// Pour rendre une propriété de classe inaccessible, ni via getter ni setter ; on va utiliser le mot clef 'private' 
class User(var email: String, private var passwird: String, var age: Int)
```

## Structures de contrôles

### Ressources

- https://openclassrooms.com/fr/courses/5353106-initiez-vous-a-kotlin/5354814-gerez-des-choix-et-des-conditions
- https://kotlinlang.org/docs/reference/control-flow.html

### La condition if

En Kotlin, la condition 'if' est une expression et renvoie donc un résultat. Elle remplace même l'expression **ternaire** en Java.

```kotlin
var a = 10
var b = 12

val result = if (a > b){
    a++
    a
} else {
    b++
    b
}

print("Result is : $result") // 13
```

### Choix multiples

En Kotlin, when remplace le switch en Java

```kotlin
var apiResponse = 404

when (apiResponse) {
    200 -> print("OK")
    404 -> print("NOT FOUND")
    401 -> print("UNAUTHORIZED")
    403 -> print("FORBIDDEN")
    else -> print("UNKNOWN")
}
```

C'est une structure-expression, elle renvoie donc un résultat

```kotlin
fun printResponse(apiResponse: Int) = when (apiResponse) {
    200 -> print("OK")
    404 -> print("NOT FOUND")
    401 -> print("UNAUTHORIZED")
    403 -> print("FORBIDDEN")
    else -> print("UNKNOWN")
}
```

Le default du switch est géré de la sorte :

```kotlin
val apiResponse = 200

when (apiResponse) {
    200, 201, 202 -> print("SUCCESS")
    300, 301, 302 -> print("REDIRECTION")
    400, 404 -> print("ERROR")
    else -> print("UNKNOWN")
}

// Ou même encore comme ça
val apiResponse = 200

when {
    isSuccess(apiResponse) -> print("SUCCESS")
    isError(apiResponse) -> print("ERROR")
}

fun isSuccess(apiResponse: Int) = apiResponse = 200 || apiResponse = 201 || apiResponse = 202
fun isError(apiResponse: Int) = apiResponse = 400 || apiResponse = 404
```

### Les énumérations

Proche de Java, voici comment en déclarer une :

```kotlin
enum class ApiResponse {
    OK,
    NOT_FOUND,
    UNAUTHORIZED,
    FORBIDDEN,
    UNKNOWN
}
```

Cette structure s'adapte bien avec la structure 'when' :

```kotlin
// On peut aussi affecter des propriétés aux énumérations

enum class ApiResponse(cal code: Int) {
    OK(200),
    NOT_FOUND(404),
    UNAUTHORIZED(401),
    FORBIDDEN(403),
    UNKNOWN(0)
}

// Nous pouvons l'utiliser ainsi avec 'when'
val response: ApiResponse = ApiResponse.OK

when(response){
    ApiResponse.OK -> print("OK")
    ApiResponse.NOT_FOUND -> print("NOT_FOUND")
    ApiResponse.UNAUTHORIZED -> print("UNAUTHORIZED")
    ApiResponse.FORBIDDEN -> print("FORBIDDEN")
    ApiResponse.UNKNOWN -> print("UNKNOWN")
}
```

Il est aussi possible d'utiliser 'when' pour les intervalles :

```kotlin
val numberToFind = 55

when(numberToFind) {
    in 1..33 -> print("Number is between 1 and 33")
    in 34..66 -> print("Number is between 34 and 66")
    in 67..100 -> print("Number is between 67 and 100")
}
```

## Les boucles

### While

```kotlin
// Pas de changement avec Kotlin
// While 
while (isARainyDay) {
    println("I don't love rain...")
}

// Do While
do {
    println("I don't love rain...")
} while (isARainyDay) 	
```

### for

```kotlin
// Déclaration d'une liste
val names = listOf("Jake Wharton", "Joe Birch", "Robert Martin");

// Parcours de la liste - <item> in <elements>
for (name in names) {
    println("This developer rocks : $name");
}
```

#### Indices dans les boucles for

```kotlin
for (i in names.indices) {
    println("This developer with number $i rocks : ${names[i]}")
}

for ((index, value) in names.withIndex()) {
    println("This developer with number $index rocks : $value")
}
```

#### Intervalles dans les boucles for

Avec Kotlin, les intervalles sont toujours inclusifs, cela signifie que les valeurs de l'intervalle sont inclus dans le parcours de la boucle.

```kotlin
for (i in 1..42) {
    println("I love this number : $i")
}
```

#### Saut dans le parcours d'un intervalle

```kotlin
for (i in 10 downTo 1 step 2) {
    println("Index is :$i")
}
// 10, 8, 6, 4, 2

for (i in 1..10 step 2) {
    println("Index is :$i")
}
// 1, 3, 5, 7, 9
```

## Les structures de données

Voici les différentes structures possibles, avec Kotlin

- [`listOf`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/list-of.html) : Permet de créer une liste d’éléments **ordonnée** et **immuable**.
- [`mutableListOf`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/mutable-list-of.html) : Permet de créer une liste d’éléments **ordonnée** et **muable**.
- [`setOf`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/set-of.html) : Permet de créer une liste d’éléments **désordonnée** et **immuable**.
- [`mutableSetOf`](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/mutable-set-of.html) : Permet de créer une liste d’éléments **désordonnée** et **muable**.
- arrayOf : pour créer un tableau de valeurs
- mapOf : pour créer un dictionnaire de valeurs

```kotlin
// listOf
val listOfNames = listOf("Jake Wharton", "Joe Birch", "Robert Martin")
listOfNames[0] // => Jake Wharton
listOfNames[0] = "Mathieu Nebra" // => ERROR ! List is immutable

// mutableListOf
val listOfNames = mutableListOf("Jake Wharton", "Joe Birch", "Robert Martin")
listOfNames[0] // => Jake Wharton
listOfNames[0] = "Mathieu Nebra" // => SUCCESS !

// setOf
val setOfNames = setOf("Jake Wharton", "Joe Birch", "Robert Martin")
listOfNames.first() // => Jake Wharton
listOfNames.add("Mathieu Nebra") // => ERROR ! Set is immutable

// mutableSetOf
val setOfNames = mutableSetOf("Jake Wharton", "Joe Birch", "Robert Martin")
listOfNames.first() // => Jake Wharton
listOfNames.add("Mathieu Nebra") // => SUCCESS !
```

## Smart Cast ou transtypage intelligent

### Ressources

- https://openclassrooms.com/fr/courses/5353106-initiez-vous-a-kotlin/5354816-decouvrez-le-smart-cast
- https://kotlinlang.org/docs/reference/typecasts.html

Ici, nous allons aboder le cast d'objet. Il faut tout d'abord savoir que tous les objets en Kotlin dérivent de la classe Any !

### Smart Cast - is

```kotlin
private fun getDefaultSize(anyObject: Any): Int {
    // Vérification du type 
    if (anyObject is String) {
        return anyObject.length
    // Vérification du type 
    } else if (anyObject is List<*>) {
        return anyObject.size
    }
    return 0 
}

// Autre manière de faire avec la structure 'when'
private fun getDefaultSize(anyObject: Any) = when (anyObject) {
    is String -> anyObject.length
    is List<*> -> anyObject.size
    else -> 0
}
```

Aussi, le smart cast est implicite avec le mot clef 'is'. 

### Smart Cast - as (unsafe)

Il est possible de transtyper avec le mot-clef 'as'. Toutefois, cette technique est appellée 'unsafe' car non sécurisée. En effet, si le cast ne fonctionne pas, alors il y aura une exception de type 'ClassCastException' :

```kotlin
val anyObject: Any = "Hello Kotlin students !"
val message = anyObject as String
print(message)
```

#### 1er contournement possible - try/catch

```kotlin
val anyObject: Any = "Hello Kotlin students !"

try{
    val message = anyObject as Int
    print(message)
    
} catch(excetpion: ClassCastException) {
    print("Error !")
}
```

#### 2nd contournement possible - ?

```kotlin
val anyObject: Any = "Hello Kotlin students !"
val message: Int? = anyObject as? Int
print(message)
```

## Gestion des exceptions

### Ressources

- https://en.wikipedia.org/wiki/Elvis_operator
- https://openclassrooms.com/fr/courses/5353106-initiez-vous-a-kotlin/5354817-maitrisez-les-exceptions
- https://kotlinlang.org/docs/reference/exceptions.html
- https://kotlinlang.org/docs/reference/null-safety.html#elvis-operator

Exemple pour le lever une exception dans un cas particulier :

```kotlin
private fun substractNumber(a: Int, b: Int) = if (a >= b) a - b else throw Exception("a is smaller than b !")
```

Toutefois, lorsqu'on va appeler cette méthode, Kotlin ne nous obkige pas comme en Java, à vérifier cette exception. Voici ce que l'on peut faire avec Kotlin :

```kotlin
// Exception non gérée 
substractNumber(20,10)

// Exception gérée 
try{
    substractNumber(20,10)
} catch(e: Exception) {
    print(e.message)
}

// Aussi, un autre cas possible
val result = try { substractNumber(10,20) } catch (é: Exception) { 0 }
print(result) //0
```

Les 'try/catch' & les exceptions sont des expressions, elles peuvent donc renvoyer un résultat et il est possible de l'exploiter pour gérer des cas particuliers :

```kotlin
// Ici, le constructeur accepte des paramètres potentiellement null
class User(val email: String?, val password: String?)
val user = User("toto@gmail.com", "azerty")
```

Nous pourions lever une exception si le mot de mot de passe est null, grâce à l'opérateur Elvis qui permet de définir une action alternative si la variable est null :

```kotlin
// ?: Opérateur Elvis
val password = user.password ?: throw IllegalArgumentEcveption("Password required")
```

Au lieu de lever à chaque fois la même exception, il est alors possible d'isoler la levée de cette exception dans une fonction spécifique, renvoyant un objet Nothing. Une exception renvoie un retour de type 'Nothing' :

```kotlin
// Nothing est le Type retourné d'une expression "throw"
private fun fail(message: String): Nothing = throw IllegalStateException(message)

val password = user.password ?: fail("Password required")
val email = user.email ?: fail("Email required")
```

