# Spring Boot

## Programme

- Spring -> https://openclassrooms.com/fr/courses/4504771-simplifiez-le-developpement-dapplications-java-avec-spring
- Spring security -> https://openclassrooms.com/fr/courses/7137776-securisez-votre-application-web-avec-spring-security
- Spring DATA/JPA -> https://openclassrooms.com/fr/courses/6982461-utilisez-spring-data-pour-interagir-avec-vos-bases-de-donnees
- Spring Cloud (micro services) -> https://openclassrooms.com/fr/courses/4668216-optimisez-votre-architecture-microservices

## Présentation

### Les starters

Spring Framework se découpe en de nombreux composants ; les utiliser implique de renseigner les bonnes dépendances pour notre projet. Ce n’est pas facile, car il faut savoir quelle dépendance est nécessaire à quelle autre dépendance, s’il y a des incompatibilités, et quelles sont les versions à utiliser.

Pour résoudre cette problématique, Spring Boot nous offre les starters de dépendances qui sont des kits de dépendances, clefs en main & prêt à l'emploi.

Exemple de starters :

- spring-boot-starter-core ;
- spring-boot-starter-data-jpa ;
- spring-boot-starter-security ;
- spring-boot-starter-test ;
- spring-boot-starter-web.

### Les annotations

Nous avons vu ensemble que Spring Framework fournit l’IoC container, dans la partie 1 du cours. L’IoC container va instancier pour vous des classes, puis si nécessaire les injecter dans d’autres instances de classe. Pour qu’une classe soit manipulée par l’IoC container, **il est nécessaire de l’indiquer explicitement à Spring**.

1. Soit via un fichier XML
2. Soit via des annotations dans le code de l'application

### IoC Container



## Création d'un projet Spring

Pour créer un projet Spring, il existe deux solutions, ou en ligne avec [Spring Initializr](https://start.spring.io/). Ou alors avec l'IDE STS [Spring Tool Suite](https://spring.io/tools).

Les deux font la même chose, excepté qu'avec STS, il est possible de débugger, monitorer les applications BOOT, etc...

### La structure du projet

- **mvnw** & **pom.xml** pour la config Maven
- DemoApplication.java est la classe de configuration contenant l'annotation **@SpringBootAppalication** 
- **application.properties** 
- Une classe de test

![image-20220117201339948](/home/yassine/.config/Typora/typora-user-images/image-20220117201339948.png)

Cette structure est minimale mais certes fonctionnelle.

### Configuration du projet

Le fichier application.\[properties]\[yaml] permet de configurer l'application. Voir le lien ci-dessous pour voir tous les attributs possibles.

ressources :

- https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html

## Architecture standard d'un projet Spring BOOT

Il convient de créer au minimum les 4 packages suivants : controller, service, repository, model

![image-20220117203221307](/home/yassine/.config/Typora/typora-user-images/image-20220117203221307.png)