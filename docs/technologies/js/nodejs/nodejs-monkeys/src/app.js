const express = require('express');
let monkeys = require('./Monkeys');

console.log("start up de ma première app");

const app = express();
const port = 3000;

app.get('/', (req, res) => res.send("Hello, the app is started :p "));

app.get('/api/monkeys', (req, res) => {
    res.send(`Hello, pour l'instant, il y a ${monkeys.length} singes, dans la singerie`);
});

app.get('/api/monkeys/:monkeyId/:bananaId', (req, res) => {
    const monkeyId = parseInt(req.params.monkeyId);
    const bananaId = req.params.bananaId;

    const monkey = monkeys.find(monkey => monkey.id === monkeyId);

    res.send(`Hello, monkey # ${monkeyId} & banana ${bananaId} has the name ${monkey.name}`);
});

app.listen(port, () => console.log(`Notre app a démarré sur : http://localhost:${port}`));
