// Récupération des pièces depuis le fichier JSON
const reponse = await fetch('pieces-autos.json');
const pieces = await reponse.json();

// boucle sur toutes les pièces
for (let piece of pieces) {
    // construction de l'objet
    const imageElement = document.createElement("img");
    imageElement.src = piece.image;
    
    const nomElement = document.createElement("h2");
    nomElement.innerText = piece.nom;
    
    const prixElement = document.createElement("p");
    prixElement.innerText = `Prix: ${piece.prix} ${piece.prix > 35 ? "€€€" : "€"}`;
    
    const categorieElement = document.createElement("p");
    categorieElement.innerText = piece.categorie ?? "aucune catégorie à afficher";
    
    // insertion de l'objet dans le DOM par le js
    const sectionFiches = document.querySelector(".fiches");

    // Création d’une balise dédiée à une pièce automobile
    const pieceElement = document.createElement("article");
    
    sectionFiches.appendChild(pieceElement);

    pieceElement.appendChild(imageElement);
    pieceElement.appendChild(nomElement);
    pieceElement.appendChild(prixElement);
    pieceElement.appendChild(categorieElement);    
};

/**
 * Filtrer par le prix
 */
const priceFilter = document.getElementById('priceFilter');
priceFilter.addEventListener('click', function() {
    console.log("Filtre par prix !");
    const piecesFiltrees = pieces.filter(function(p) {
        return p.prix <= 35;
    });
    console.log(piecesFiltrees);
});

/**
 * Trier par le prix
 */
const sortByPrice = document.getElementById('sortByPrice');
sortByPrice.addEventListener('click', function() {
    console.log("Tri par prix !");

    const pieceTrieesParPrix = Array.from(pieces);
    pieceTrieesParPrix.sort(function(a, b) {
        return a.prix - b.prix;
    });
    console.log(pieceTrieesParPrix);
});

/**
 * Afficher le nom des pièces abordables
 */
const displayPiecesAbordables = document.getElementById('displayPiecesAbordables');
displayPiecesAbordables.addEventListener('click', function() {
    console.log("Affihcer le nom des pièces abordables");

    // on récupère tous les noms de pièces
    const noms = pieces.map(piece => piece.nom);

    // on va maintenant supprimer le nom des pièces ayant un prix > 35 €
    for (let i = noms.length -1; i >= 0; i--) {
        if (pieces[i].prix > 35) {
            noms.splice(i, 1);
        }
    }

    // affichage de la liste de pièces abordables
    console.log(noms);

    // display on UI now
    const abordablesElt = document.querySelector(".abordables");

    // création du titre de la section
    const abordablesTitle = document.createElement("h2");
    abordablesTitle.innerText = "Liste des pièces abordables";

    abordablesElt.appendChild(abordablesTitle);

    const nomPieceList = document.createElement("ul");
    
    for (let nom of noms) {
        const listElt = document.createElement("li");
        listElt.innerText = nom;
        nomPieceList.appendChild(listElt);
    }

    abordablesElt.appendChild(nomPieceList);

});

const disponibleElt = document.getElementById("displayPiecesDisponibles");
disponibleElt.addEventListener('click', function() {

    const dispoSection = document.querySelector(".disponibles");

    // construction du titre de la section
    const dispoTitle = document.createElement("h2");
    dispoTitle.innerText = "Liste des pièces disponibles";

    dispoSection.appendChild(dispoTitle);

    const pieceNomEtPrix = pieces.map(piece => piece.nom + " - " + piece.prix + " €");

    for (let i = pieces.length - 1; i >= 0; i--) {
        if (! pieces[i].disponibilite) {
            pieceNomEtPrix.splice(i, 1);
        }
    }

    const nomPieceList = document.createElement("ul");
    
    for (let nom of pieceNomEtPrix) {
        const listElt = document.createElement("li");
        listElt.innerText = nom;
        nomPieceList.appendChild(listElt);
    }

    dispoSection.appendChild(nomPieceList);l

    console.log(pieceNomEtPrix);
});
