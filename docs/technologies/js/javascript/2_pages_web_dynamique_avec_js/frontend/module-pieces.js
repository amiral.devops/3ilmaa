/**
 * ce code est venu après pieces.js
 * pour apporter plus de modularité.
 */

import { ajoutListenersAvis, ajoutListenerEnvoyerAvis } from "./avis.js";

// Récupération des pièces éventuellement stockées dans le localStorage
let pieces = window.localStorage.getItem("pieces");
if (pieces === null) {
    const response = await fetch('http://localhost:8081/pieces').then(pieces => pieces.json());

    const valeurPieces = JSON.stringify(response);
    window.localStorage.setItem("pieces", valeurPieces);
} else {
pieces = JSON.parse(pieces);
}

ajoutListenerEnvoyerAvis();

// insertion de l'objet dans le DOM par le js
const sectionFiches = document.querySelector(".fiches");


generateView(pieces);


function generateView(pieceList) {
    resetView();
    for (let piece of pieceList) {
        // construction de l'objet
        const imageElement = document.createElement("img");
        imageElement.src = piece.image;
        
        const nomElement = document.createElement("h2");
        nomElement.innerText = piece.nom;
        
        const prixElement = document.createElement("p");
        prixElement.innerText = `Prix: ${piece.prix} ${piece.prix > 35 ? "€€€" : "€"}`;
        
        const categorieElement = document.createElement("p");
        categorieElement.innerText = piece.categorie ?? "aucune catégorie à afficher";
        
        //Code ajouté pour récupérer les avis attachés aux pièces
        const avisBouton = document.createElement("button");
        avisBouton.dataset.id = piece.id;
        avisBouton.textContent = "Afficher les avis";

        // Création d’une balise dédiée à une pièce automobile
        const pieceElement = document.createElement("article");
                
        pieceElement.appendChild(imageElement);
        pieceElement.appendChild(nomElement);
        pieceElement.appendChild(prixElement);
        pieceElement.appendChild(categorieElement);
        pieceElement.appendChild(avisBouton);
        
        sectionFiches.appendChild(pieceElement);
    }
    ajoutListenersAvis();
}

const sortByPriceBut = document.getElementById("sortByPrice");
sortByPriceBut.addEventListener('click', function() {
    resetView();

    const piecesSorted = Array.from(pieces);
    piecesSorted.sort(function(a, b) {
        return a.prix - b.prix;
    });

    generateView(piecesSorted);

});

const resetBut = document.getElementById('resetList');
resetBut.addEventListener('click', function() {
    resetView();

    generateView(pieces);
});


function resetView() {
    sectionFiches.innerHTML = '';
}

const abordableBut = document.getElementById('displayPiecesAbordables');
abordableBut.addEventListener('click', function() {
    generateView(filterPiecesByPrice(35));
});

const dispoBut = document.getElementById('displayPiecesDisponibles');
dispoBut.addEventListener('click', function() {

    const piecesDispos = Array.from(pieces);
    for (let i = pieces.length - 1; i >= 0; i--) {
        if (! pieces[i].disponibilite) {
            piecesDispos.splice(i, 1);
        }
    }

    generateView(piecesDispos);

});


const cursorBut = document.getElementById('priceCursor');
cursorBut.addEventListener('change', function() {
    generateView(filterPiecesByPrice(cursorBut.value));
});

function filterPiecesByPrice(priceTarget) {
    const filteredList = Array.from(pieces);

    for (let i = pieces.length -1; i >= 0; i--) {
        if (pieces[i].prix > priceTarget) {
            filteredList.splice(i, 1);
        }
    }
    return filteredList;
}
