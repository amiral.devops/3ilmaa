# JavaScript pour le Web

## Ressources

- Source du cours -> https://openclassrooms.com/fr/courses/5543061-ecrivez-du-javascript-pour-le-web 
- Documentation Mozilla :
  -  https://developer.mozilla.org/fr/docs/Web/API/Element
  - https://developer.mozilla.org/fr/docs/Web/API/Document

## Le DOM (Document Object Model)

Voici une représentation du HTML qui peut-être vu comme un arbre. Le DOM est une interface de programmation, permettant d'accéder et de gérer les éléments de l'arbre HTML, via le langage JavaScript.

![Le DOM commence avec un head et un body puis un arbre de balises se forme à l'intérieur du body pour représenter le document](https://user.oc-static.com/upload/2021/04/20/1618926723843_1c1_arbre-DOM.png)

### L'objet JavaScript 'document'

Cet objet, directement disponible dans le code JavaScript, permet d'accéder à toute la page HTML et tout particulièrement de récupérer n'importe quel élément, via ses fonctions.

### document.getElementById()

C'est une fonction qui permet de récupérer un élément par son ID :

```html
<p id="my-anchor">My content</p>
```

```javascript
const myAnchor = document.getElementById('my-anchor');
```

### document.getElementByClassName()

C'est une fonction qui permet de récupérer un élément par sa classe :

```html
<div>
    <div class="content">Contenu 1</div>
    <div class="content">Contenu 2</div>
    <div class="content">Contenu 3</div>
</div>
```

```javascript
const contents = document.getElementsByClassName('content');
const firstContent = contents[0]; // firstContent contient la valeur 'Contenu 1'
```

### document.getElementsByTagName()

Cette fonction permet de récupérer tous les éléments par le nom de la balise HTML :

```html
<div>
    <article>Contenu 1</article>
    <article>Contenu 2</article>
    <article>Contenu 3</article>
</div>
```

```javascript
const articles = document.getElementsByTagName('article');
const thirdArticle = articles[2]; // thirdArticle contient la valeur 'Contenu 3'
```

### document.querySelector()

C'est une fonction qui est plus complexe mais qui permet de rechercher plus finement un élément dans le DOM :

```html
<div id="myId">
    <p>
        <span><a href="#">Lien 1</a></span>
        <a href="#">Lien 2</a>
        <span><a href="#">Lien 3</a></span>
    </p>
    <p class="article">
        <span><a href="#">Lien 4</a></span>
        <span><a href="#">Lien 5</a></span>
        <a href="#">Lien 6</a>
    </p>
    <p>
        <a href="#">Lien 7</a>
        <span><a href="#">Lien 8</a></span>
        <span><a href="#">Lien 9</a></span>
    </p>
</div>
```

Le pattern est [id] [tag].[class] > [tag].

Il est à noter que cette fonction renverra le premier élément trouvé et donc pas une liste. cette fonction peut prendre aussi en paramètre un sélecteur qui renverra le résultat.

```javascript

const elt = document.querySelector("#myId p.article > a"); // elt contient la valeur 'Lien 6'
```

### Parcours des éléments

```html
<div id="parent">
    <div id="previous">Précédent</div>
    <div id="main">
        <p>Paragraphe 1</p>
        <p>Paragraphe 2</p>
    </div>
    <div id="next">Suivant</div>
</div>
```

```javascript
const elt = document.getElementById('main');
const children = elt.children; // renverra les éléments enfants <p>
const parent = elt.parentElement; // renverra la div avec l'id 'parent'
const nextElt = elt.nextElementSibling; // renverra la div avec l'id 'next'
const previousElt = elt.previousElementSibling; // renverra la div avec l'id 'previous'
```

### Exercice

Source de l'exercice -> https://openclassrooms.com/fr/courses/5543061-ecrivez-du-javascript-pour-le-web/5577476-accedez-aux-elements-du-dom#/id/r-5796282

```html
<article>
  <p class="important">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</article>
<article>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </p>
  <ul>
    <li>Elément 1</li>
    <li>Elément 2</li>
    <li>Elément 3</li>
  </ul>
  <p id="main-content">Ce contenu est spécial.</p>
  <ul class="important">
    <li>Elément 4</li>
    <li>Elément 5</li>
    <li>Elément 6</li>
  </ul>
</article>
<article>
  	<p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      <ul>
        <li>Elément 7</li>
        <li>Elément 8</li>
        <li>Elément 9</li>
      </ul>
  	</p>
</article>
<p class="important">
  	Il n'y a plus d'article
    <ul>
      <li>Elément 10</li>
      <li>Elément 11</li>
      <li>Elément 12</li>
    </ul>
</p>
<article>
</article>
```

```javascript
let listeEl = document.querySelector("article > ul.important > li")

console.log(listeEl.nextElementSibling);

let importants = document.getElementsByClassName("important");
console.log(importants.length);// return 3

let articles = document.getElementsByTagName('article');
console.log(articles.length);// return 4

const elt = document.querySelector("article > ul.important > li");
console.log(elt);// return <li>Elément 4</li>

let nextSibElt = elt.nextElementSibling;
console.log(nextSibElt); // return <li>Elément 5</li>
```

## Modification du DOM

### Ressources

- https://developer.mozilla.org/fr/docs/Web/API/Node
- https://developer.mozilla.org/fr/docs/orphaned/Web/API/ElementCSSInlineStyle/style

### Ajouter du contenu

Il existe deux moyens de modifier le DOM. Les propriétés innerHTML & textContent permettent de modifier l'élément actuel, en prenant respectivement du code HTML ou du contenu texte.

```javascript
let elt = document.getElementById('main');
elt.innerHTML = "<ul><li>Elément 1</li><li>Elément 2</li></ul>";

// l'élément 'main' deviendra :
<div id="main">
    <ul>
        <li>Elément 1</li>
        <li>Elément 2</li>
    </ul>
</div>
```

### Modifier les classes

La propriété classList permet de modifier les classes de l'élément actuel.

```javascript
elt.classList.add("nouvelleClasse");    // Ajoute la classe nouvelleClasse à l'élément
elt.classList.remove("nouvelleClasse"); // Supprime la classe nouvelleClasse que l'on venait d'ajouter
elt.classList.contains("nouvelleClasse");   // Retournera false car on vient de la supprimer
elt.classList.replace("oldClass", "newClass"): // Remplacera oldClass par newClass si oldClass était présente sur l'élément
```

### Modifier les styles d'un élément

Il est possible d'effectuer des changements du style de l'élément, via l'objet **style** :

```javascript
elt.style.color = "#fff";      // Change la couleur du texte de l'élément à blanche
elt.style.backgroundColor = "#000"; // Change la couleur de fond de l'élément en noir
elt.style.fontWeight = "bold"; // Met le texte de l'élément en gras
```



### Modifier les attributs

Les fonctions **setAttribute(), getAttribute() et removeAttribute**() de l'objet Element, permettent de gérer les attributs des éléments.

```javascript
elt.setAttribute("type", "password");   // Change le type de l'input en un type password
elt.setAttribute("name", "my-password");    // Change le nom de l'input en my-password
elt.getAttribute("name");               // Retourne my-password
elt.removeAttribute("type");			// Supprime l'attribut type
```

### Créer de nouveaux éléments

La fonction **createElement()** de l'objet **Document**, permet de créer de nouveaux éléments, en prenant le type en paramètre.

```javascript
/*
	La variable newElt contient un bloc 'div'.
	Toutefois, cet élément ne sera pas encore visible, il faudra pour se faire, le rajouter comme enfant Cf. section suivante.
*/
const newElt = document.createElement("div"); 
```

### Ajouter des enfants

La fonction **appendChild()** de l'objet **Node**, permet de rajouter l'élément récemment créé dans le dom.

```javascript
const newElt = document.createElement("div");
let elt = document.getElementById("main");

elt.appendChild(newElt);
```

### Supprimer & remplacer des éléments

L'objet **Node** propose également d'autres fonctions pour gérér les éléments :

```javascript
const newElt = document.createElement("div");
let elt = document.getElementById("main");
elt.appendChild(newElt);

elt.removeChild(newElt);    // Supprime l'élément newElt de l'élément elt
elt.replaceChild(document.createElement("article"), newElt);    // Remplace l'élément newElt par un nouvel élément de type article
```

### Exercice

SOurce de l'exercice -> https://openclassrooms.com/fr/courses/5543061-ecrivez-du-javascript-pour-le-web/5577491-modifiez-le-dom#/id/r-6845303

```javascript
let pElt = document.createElement("p"); 
let mainElt = document.getElementById("main");

mainElt.appendChild(pElt);

pElt.innerHTML = "Mon <strong>grand</strong> contenu";
pElt.classList.add("important");

pElt.style.color = "green";
```

## Les évènements

### Ressources

- https://developer.mozilla.org/fr/docs/Web/API/EventTarget/addEventListener
- https://developer.mozilla.org/fr/docs/Web/Events

### Définition d'un évènement

En JavaScript, un évènement est composé d'un nom et d'une fonction, appellée callback qui va gérer cet évènement.

```javascript
const elt = document.getElementById('mon-lien');    // On récupère l'élément sur lequel on veut détecter le clic
elt.addEventListener('click', function() {          // On écoute l'événement click
    elt.innerHTML = "C'est cliqué !";               // On change le contenu de notre élément pour afficher "C'est cliqué !"
});
```

### Ecoute d'un évènement

#### preventDefault()

Certains élément que l'on pourrait écouter, ont un comportement par défaut, comme un lien par exemple. Le lien par défault du lien est de renvoyer l'utilisateur vers une autre page. Il est alors possible de prévenir le comportement par défault avec la méthode preventDefault() de l'objet event, transmis en paramètre de la callback.

```javascript
const elt = document.getElementById('mon-lien');    // On récupère l'élément sur lequel on veut détecter le clic
elt.addEventListener('click', function(event) {     // On écoute l'événement click, notre callback prend un paramètre que nous avons appelé event ici
    event.preventDefault();                         // On utilise la fonction preventDefault de notre objet event pour empêcher le comportement par défaut de cet élément lors du clic de la souris
});
```



#### stopPropagation() 

Tous les éléments transmettent nativement, l'évènement à leur parent, et ainsi de suite jusqu'à l'élément racine de la cible. Pour éviter que d'autres éléments écoutent un évènement, il est alors possible d'appeler la méthode stopPropagation() de l'objet event, transmis en paramètre de la callback.

```javascript
elementInterieur.addEventListener('click', function(event) {
    event.stopPropagation();
    elementAvecMessage.innerHTML = "Message de l'élément intérieur";
});
```



### Exercice

Voir <PROJECT_HOME>/docs/technologies/javascript/src/js4web/cours4.event.listener

## Récupérer les données utilisateur avec les évènements 

### Ressources

- https://developer.mozilla.org/fr/docs/Web/Events/mousemove
- https://developer.mozilla.org/en-US/docs/Web/Events/change
- https://developer.mozilla.org/en-US/docs/Web/Events/input

### Ecouter la souris :-)

Il est possible d'avoir un objet de type MouseEvent permettant d'avoir plusieurs informations sur la souris.

```javascript
elt.addEventListener('mousemove', function(event) {
    const x = event.offsetX; // Coordonnée X de la souris dans l'élément
    const y = event.offsetY; // Coordonnée Y de la souris dans l'élément
    /*
    clientX  /  clientY  : position de la souris dans les coordonnées locales (contenu du DOM) ;
	offsetX  /  offsetY  : position de la souris par rapport à l'élément sur lequel on écoute l'événement ;
	pageX  /  pageY  : position de la souris par rapport au document entier ;
	screenX  /  screenY  : position de la souris par rapport à la fenêtre du navigateur ;
    movementX  /  movementY  : position de la souris par rapport à la position de la souris lors du dernier événement  			mousemove
    */
});
```

