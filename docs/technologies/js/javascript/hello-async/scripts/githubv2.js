const githubFormv2 = document.getElementById('github-formv2');
const githubContentv2 = document.getElementById('github-content');

githubFormv2.addEventListener('submit', lookForAccount);

// le paramètre e est le submit event
async function lookForAccount(e) {
    
    // l'appel de la fucntion preventDefault permet de ne pas rafraôchir toute la page 
    // après la soumission du formulaire
    e.preventDefault();
    const accountNamev2 = githubFormv2.elements[0].value;
    
    const githubData = await fetch(`https://api.github.com/users/${accountNamev2}`)
    const githubDataJson = await githubData.json();
    console.log(`looks good ${githubDataJson}`);
    githubContentv2.innerHTML = `<pre><code></code>${JSON.stringify(githubDataJson, null, 4)}</pre>`;
}