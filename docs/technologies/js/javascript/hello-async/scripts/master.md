# Programmation aynchrone JS

## L'objet Promise & avantages

***

Une page Web, gère plusieurs composants graphiques qui eux-mêmes gèrent plusieurs évènements. Lorsqu'une opération est en cours, il n'est donc pas envisageable de bloquer toute la page en attendant la fin du traitement.

Il faut garder à l'esprit que dans les sites Web, l'expérience utilisateur est importante et l'utilisateur n'est pas prêt à attendre le chargement de la page plus de 200ms (12 Fevrier 2023).

C'est pourquoi ii y a le concept de promesse en js qui permet l'exécution du code de manière asynchrone. 

Ce sont les fonctions qui renvoients des promesses. On appelle donc une telle fonction et on gère la promesse dès que celle-ci est résolue de manière positive ou négative.

## Gestion d'une promesse

***

### then & catch

***

Cette façon de faire a été chronologiquement la première.

#### Création d'une promesse

``` javascript
// cette fonction renvoie une promesse
function startDecisionProcess() {
    console.log('the recuiter is processing others candidates');

    // une promesse prend en param deux autres fonctions resolve & reject
    return new Promise((resolve, reject) => {

        // on simule ici une action qui prend du temps
        setTimeout(() => {

            // on implémente les conditions du succès de la promesse
            if (candidat.isProgrammer && candidat.isCool) {
                console.log('just before resolve');
                resolve('ok that sounds good for the future');
            } else {
                console.log('just before resolve');
                reject('despite your background... you\'re not accepted');
            }
        }, 3000);
    })
}
```

#### Gestion de la promesse

``` javascript

    // déclaration de la fonction qui doit gérer une promesse
    function processCandidate() {

        console.log('hi there!');
        // on appelle ici la fonction précédente qui renvoie donc une promesse
        startDecisionProcess()
        
        // on peut chaîner l'appel avec un then, voire d'autres à la suite
        .then(result => {
            console.log(`status -> ${result}`);
        })
        
        // et on chaine un catch pour gérer le cas d'erreur
        .catch(err => {
            console.log(`status -> ${err}`);
        });
    }
```

### await & async

***

Cette nouvelle façon de faire a vu le jour en 2017. La fonction qui renvoie une promesse ne change pas. Toutefois, le changement intervient sur la façon de gérer la promesse.

``` javascript

    // une fonction qui gère un appel asynchrone se voit préfixée par 'async'
    async function processCandidate() {
        console.log('hi there!');

        // on place un try car l'appel asynchrone peut entraîner une exception
        try {

            // on place le mot-clef 'await' juste avant l'appel asynchrone
            const result = await startDecisionProcess();
            console.log(`result -> ${result}`);

        // le cas d'erreur est géré par un catch 
        } catch (err) {
            console.log(`result -> ${err}`);
        }
        console.log('the process ended there');
    }
```

A chaque fois qu'il y a du 'then' alors il faut un await.
