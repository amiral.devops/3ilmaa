
const btnApply = document.getElementById('btn-apply');

btnApply.addEventListener('click', processCandidate);

function processCandidate() {
    console.log('hi there!');
    startDecisionProcess().then(result => {
        console.log(`status -> ${result}`);
    }).catch(err => {
        console.log(`status -> ${err}`);
    });
}

console.log('une exécution en dehors du process');

const candidat = {
    isProgrammer : false,
    isCool : true
}

function startDecisionProcess() {
    console.log('the recuiter is processing others candidates');
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (candidat.isProgrammer && candidat.isCool) {
                console.log('just before resolve');
                resolve('ok that sounds good for the future');
            } else {
                console.log('just before resolve');
                reject('despite your background... you\'re not accepted');
            }
        }, 3000);
    })
}
