import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { ComputeService } from './services/compute/compute.service';

describe('AppComponent', () => {
  // le mirroir du composant
  let fixture: ComponentFixture<AppComponent>;

  // instance du composant
  let app: AppComponent;
  let service: ComputeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      schemas: [NO_ERRORS_SCHEMA]
    });

    // affectation des variables dans le beforeEach
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    service = TestBed.inject(ComputeService);
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it('should change age after changeAge have been called', () => {
    expect(app.age).toBe(1);

    app.changeAge();

    expect(app.age).toBe(73);
  });

  it('should check if app-accounting is present', () => {

    // ici nous récupérons tous les éléments HTML du DOM du composant à tester
    const elements = fixture.debugElement;

    // nous fesons une requête css pour récupérer le composant fils
    const accounting = elements.query(By.css('app-accounting'));

    // enfin, nous checkons que le composant a bien été chargé en mémoire
    expect(accounting).toBeTruthy();
  });

  it('should add input data binding correctly', () => {
    // ici nous récupérons tous les éléments HTML du DOM du composant à tester
    const elements = fixture.debugElement;

    // nous fesons une requête css pour récupérer le composant fils
    const accounting = elements.query(By.css('app-accounting'));

    // on demande à Angular de détecter les changements dans le DOM
    fixture.detectChanges();

    // on teste l'input en allant chercher la property
    expect(accounting.properties['amount']).toBe(4);    
  });

  it('should add output data has changed correctly', () => {
    expect(app.currentAmount).toBe(0);

    // ici nous récupérons tous les éléments HTML du DOM du composant à tester
    const elements = fixture.debugElement;

    // nous fesons une requête css pour récupérer le composant fils
    const accounting = elements.query(By.css('app-accounting'));

    // on demande à Angular de simuler l'event-emitter et on lui associe une valeur
    accounting.triggerEventHandler('amountChanged', 10);

    expect(app.currentAmount).toBe(10);    
  });

  describe('module calc', () => {
    let spyIsValidNumber: jasmine.Spy;
    
    it('should multiply two numbers', () => {
      expect(app.calc(4,5)).toBe(20);
    });
    
    it('should verify that isValidNumber have been called', () => {
      spyIsValidNumber = spyOn(service, 'isValidNumber').and.returnValue(true);
      
      const spyValue: jasmine.Spy = spyOnProperty(service, 'value', 'set');;
      app.calc(4,5);

      expect(spyIsValidNumber).toHaveBeenCalledTimes(2);
      expect(spyValue).toHaveBeenCalled();
    });
  });
});

