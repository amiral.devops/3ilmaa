import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ComputeService {

  private _value: number = 0;

  constructor() { }


  public get value() {
    return this._value;
  }

  public set value(val: number){
    this._value = val;
  }

  isValidNumber(a: any) {
    if ((a != null || a != undefined) && !isNaN(a)) {
      return true;
    }
    return false;
  }
}
