import { TestBed } from '@angular/core/testing';

import { ComputeService } from './compute.service';

describe('ComputeService', () => {
  let service: ComputeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComputeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should check if b is a valid number', () => {
    let b;
    expect(service.isValidNumber(b)).toBe(false);
  });

  it('should check if 5 is a valid number', () => {
    let b = 5;
    expect(service.isValidNumber(b)).toBe(true);
  });

  it('should check if null is a valid number', () => {
    let b = null;
    expect(service.isValidNumber(b)).toBe(false);
  });

  it("should check if '5' is a valid number", () => {
    let b = '5';
    expect(service.isValidNumber(b)).toBe(true);
  });
});
