# Concurrence

- [Concurrence](#concurrence)
  - [Ressources](#ressources)
  - [Goroutines](#goroutines)
  - [Channels](#channels)


## Ressources

- [Interchain Academy](https://ida.interchain.io/tutorials/4-golang-intro/7-concurrency.html)
- Go by example

## Goroutines

Il y a deux concepts : concurrency & parallelism. La concurrence est une architecture logicielle pour pouvoir mettre en place le parallélisme d'exécution. Quant au parallélisme, il nécessite d'une architecture physique au niveau du micro-processeur pour la rendre possible.

```go
package main

import (
    "fmt"
    "time"
)

func doSomething(size int) {
    for i := 0; i < size; i++ {
        fmt.Println(i)
        time.Sleep(time.Second)
    }
}

func main() {
    go doSomething(10) // go statement before a function creates a goroutine
    go doSomething(10)
    time.Sleep(10*time.Second)
}
```

## Channels

Ils permettent de communiquer entre les goroutines. Il y a deux types : buffered et unbuffered.

```go
// déclaration canonique
ch:= make(chan type)

// pour écrire dans un channer
ch <- valeur

//pour lire depuis un channel
valeur := <-ch
```

Exemple de code de channels unbuffered. La ligne de print attend que tous les channels soient pleins. C'est donc bloquant et contraignant.

```go
package main

import (
    "fmt"
    "time"
)

func doSomething(size int, c chan int) {
    for i := 0; i < size; i++ {
        time.Sleep(100 * time.Millisecond)
    }
    c <- size
}

func main() {
    c := make(chan int)
    go doSomething(10, c)
    go doSomething(20, c)
    go doSomething(30, c)

    x, y, z := <-c, <-c, <-c
    fmt.Println(x, y, z)        // 10 20 30
}
```

Exemple de code non bloquant

```go
package main

import (
    "fmt"
    "time"
)

func doSomething(size int, c chan int) {
    for i := 0; i < size; i++ {
        time.Sleep(100 * time.Millisecond)
    }
    c <- size
}

func doAll(c chan int) {
    d:= make(chan int)
    go doSomething(10, d)
    go doSomething(20, d)
    go doSomething(30, d)
    c <- (<-d)
    c <- (<-d)
    c <- (<-d)
    close(c)    // toujours fermer le channel avant d'itérer dessus
}

func main() {
    c := make(chan int)

    go doAll(c)
    for i := range c {
        fmt.Println(i)
    }
}
```
