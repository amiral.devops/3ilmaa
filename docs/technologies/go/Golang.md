
- [Les fonctions GO](#les-fonctions-go)
- [Les conditions](#les-conditions)
  - [Ressources](#ressources)
  - [if, else](#if-else)
  - [else if](#else-if)
  - [switch / case](#switch--case)
- [Les boucles](#les-boucles)
  - [for loop](#for-loop)
- [Les tableaux](#les-tableaux)


## Les fonctions GO

``








## Les conditions

### Ressources

- https://devopssec.fr/article/conditions-golang

### if, else

```go
package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
)

func main() {
    scanner := bufio.NewScanner(os.Stdin)
    fmt.Print("Entrez votre age : ")
    scanner.Scan()                   
    age, err := strconv.Atoi(scanner.Text())
    if err != nil {
        // Si la conversion n'a pas fonctionné alors on affiche un message d'erreur et on quitte le programme
        fmt.Println("On essaie de m'arnaquer ? allé Dehors ! Et la prochaine entrez un entier !")
        os.Exit(2) // on quitte le programmation
    }

    if age < 17 { // vérifier si l'utilisateur à au moins 18 ans
        fmt.Println("Sortez !")
    } else { // si ce n'est pas le cas alors on l'accepte pas
        fmt.Println("Entrez :)")
    }
}

// Autre forme de if
// if statement, condition
if i := 18, i < 18 {
    // code
}
```

### else if

```go
package main

import (
    "bufio"
    "fmt"
    "math/rand"
    "os"
    "strconv"
    "time"
)

func main() {
    scanner := bufio.NewScanner(os.Stdin)
    fmt.Print("Entrez votre age : ")
    scanner.Scan()
    age, err := strconv.Atoi(scanner.Text())
    if err != nil {
        fmt.Println("On essaie de m'arnaquer ? allé Dehors ! Et la prochaine entrez un entier !")
        os.Exit(2)
    }

    fmt.Print("Entrez votre prenom : ")
    scanner.Scan()
    prenom := scanner.Text()

    /*
        On a besoin de changer la graine (générateur de nombres pseudo-aléatoires) à 
        chaque exécution de programmation sinon on obtiendra le même nombre aléatoire
    */
    rand.Seed(time.Now().UnixNano())
    radomInt := rand.Intn(2) // retourne aléatoirement soit 1 soit 0
    radomInt2 := rand.Intn(2)

    if age < 18 {
        fmt.Println("Sortez !")
    } else if prenom == "Hatim" || prenom == "hatim" {
        fmt.Println("Ah c'est toi Hatim, dehors !")
    } else if age == 18 && radomInt == 1 { // si le client est chanceux et qu'il a 18 ans
        fmt.Println("Hum vous avez de la chance je suis de bonne humeur, entrez !")
    } else if radomInt2 == 0 {
        fmt.Println("Vous n'avez vraiment pas de chance sortez !")
    } else {
        fmt.Println("Entrez :)")
    }

}
```

### switch / case

```go
package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
)

func main() {
    scanner := bufio.NewScanner(os.Stdin)
    fmt.Print("Votre choix : ")
    scanner.Scan()
    choix, err := strconv.Atoi(scanner.Text())
    if err != nil {
        fmt.Println("Entrez un entier !")
        os.Exit(2)
    }

    switch choix { // la variable qu'on souhaite vérifier
        case 0, 1: // 1 ou 0
            println("George Boole !")
        case 7:
            println("William Van de Walle!")
        case 23:
            println("Pour certains, le nombre 23 est source de nombreux mystères, qu'en penses-tu Jim Carrey?")
        case 42:
            println("la réponse à la question ultime du sens de la vie!")
        case 666:
            println("Quand le diable veut une âme, le mal devient séduisant.")
        default:
            println("Mauvais choix !") // Valeur par défaut
    }
}
```

## Les boucles

### for loop

- break -> on sort complètement de la boucle
- continue -> casse l'itération courante et next sur la suivante

```go
func main() {
	fmt.Println("Nous allons voir ici les boucles :)")

	for i := 0; i < 5; i++ {
		fmt.Println(i)
	}

	println("\n 2ntest \n")

	x := 0
	for x < 5 {
		fmt.Println(x)
		x++
	}

	println("\nIntroduction du mot clef 'break' \n")
	y := 0
	for {
		if y == 5 {
			break
		}
		println(y)
		y++
	}

	println("\nIntroduction du mot clef 'continue' \n")

	p := 0
	// le ';' signifie au compilateur que la variable a été initialisée préalablement
	println("Ici, nous allons n'afficher que les nombres paires")
	for ; p < 10; p++ {
		if p%2 != 0 {
			continue
		}
		println(p)
	}
}

```

## Les tableaux

