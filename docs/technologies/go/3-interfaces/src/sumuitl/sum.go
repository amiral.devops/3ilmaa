package main

import "fmt"

func Sum(a, b int) (s int) {
	for i := a; i <= b; i++ {
		s += i
	}
	return
}

func main() {
	opa, opb := 2, 4
	fmt.Println(Sum(opa, opb))
}
