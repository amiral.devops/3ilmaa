# Interfaces

## Ressources

- [IDA interfaces](https://ida.interchain.io/tutorials/4-golang-intro/3-interfaces.html)
- [Tests en Go](https://blog.alexellis.io/golang-writing-unit-tests/)

## Implémentation d'une interface

```go
package main
import (
    "fmt"
    "math"
)

// déclaration de types
// *************************************************
type Vector3D struct {
    x, y, z float64
}
type Vector2D struct {
    x, y float64
}
type Number float64

// déclaration de l'interface Euclid
// *************************************************
type Euclid interface {
    Norm() float64
}

// implémentation de l'interface
// *************************************************
func (v Vector3D) Norm() float64 {
    return math.Sqrt(v.x*v.x + v.y*v.y + v.z*v.z)
}
func (v Vector2D) Norm() float64 {
    return math.Sqrt(v.x*v.x + v.y + v.y)
}
func (n Number) Norm() float64 {
    if n > 0 {
        return float64(n)
    }
    return -float64(n)
}

// *************************************************
func main() {
    var v Euclid
    v = Vector3D{1, 2, 3}
    fmt.Println(v.Norm())
    v = Vector2D{1, 2}
    fmt.Println(v.Norm())
    v = Number(-2.5)
    fmt.Println(v.Norm())
}
```

## Interface vide

```go
package main

import "fmt"

func main() {
    var i interface{}

    i = 2
    fmt.Println(i)          // 2

    i = "Test"
    fmt.Println(i)          // Test

    s, ok := i.(string)
    fmt.Println(s, ok)      // Test true

    t, check := i.(int)
    fmt.Println(t, check)   // 0 false
}
```

## Test basics

Il faut avoir initialiser le projet

```go
go mod init project_name
```

- il faut que le fichier de test ait le pattern xxx_test.go
- une fonction aynt le pattern TestXxx

```go
package main

import "testing"

func TestSum(t *testing.T) {
 tables := []struct {
  opa, opb, result int
 }{
  {1, 1, 2},
  {1, 2, 3},
  {2, 2, 4},
  {5, 2, 7},
 }

 for _, table := range tables {
  total := Sum(table.opa, table.opb)
  if total != table.result {
   t.Errorf("Sum of (%d+%d) was incorrect, got: %d, want: %d.", table.opa, table.opb, total, table.result)
  }
 }
}
```