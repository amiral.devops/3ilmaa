# Les tableaux

- [Les tableaux](#les-tableaux)
  - [Ressources](#ressources)
  - [Arrays](#arrays)
  - [Slices](#slices)
  - [Maps](#maps)

## Ressources

- [Interchain Academy](https://ida.interchain.io/tutorials/4-golang-intro/5-arrays.html)

## Arrays

Les tableaux ont une taille fixe.

```go
// déclaration canonique
var array [size]type

package main
import (
    "fmt"
)
func main() {
    v1 := [3]float64{7, 5, 4}
    var v2 [3]float64
    v2 = [3]float64{2, 4, 6}

    // déclaration dynamique de la taille du tableau
    for v3,i := [...]float64{0, 0, 0}, 0; i < len(v3); i++ {
        v3[i] = v1[(i + 1) % 3] * v2[(i + 2) % 3] - v1[(i + 2) % 3] * v2[(i + 1) % 3]
        defer fmt.Printf("%t\n", v3)
    }
}
```

## Slices

Les slices sont plus souples que les tableaux. Ils n'ont pas de tailles fixes et sont plus utilisés que les tableaux.

```go
// déclaration canonique
var slice []type
```

Un slice a une longueur et une capacité.

On peut

- le créer avec la fonction **make([]type, length, capacity) []type**
- le copier avec la fonction **copy(dst, src []T) int**

```go
package main
import "fmt"
func main() {
    vectors := []struct {
        x,y,z float64
    } {
        { 1, 2, 3 },
        { 3.2, 4, 6 },
        { 4, 3, 1},
    }

    // type []struct { x float64; y float64; z float64 } and value [{1 2 3} {3.2 4 6} {4 3 1}]
    fmt.Printf("type %#T and value %v\n", vectors, vectors)
    vectors = append(vectors, struct{ x, y, z float64 }{ 7, 7, 7 })
    
    // type []struct { x float64; y float64; z float64 } and value [{7 7 7}]
    fmt.Printf("type %#T and value %v\n", vectors[3:], vectors[3:])
    
    // type struct { x float64; y float64; z float64 } and value {7 7 7}
    fmt.Printf("type %#T and value %v\n", vectors[3], vectors[3])
    for i, v := range vectors {
        fmt.Println(i, " : ", v)
    }
    numbers := make([]int, 10, 10) // create a slice with an underlying array

    // [0 0 0 0 0 0 0 0 0 0]
    fmt.Println(numbers)
    for i := range numbers {
        numbers[i] = i
    }

    // [0 1 2 3 4 5 6 7 8 9]
    fmt.Println(numbers)
}
```

## Maps

C'est le principe de clef valeur

Déclaration canonique

```go
var m map[keyType]valueType // nil map

// il est préférable d'utiliser la fonction built-in
m := make(map[keyType]valueType)
```

Exemple de code utilisant des maps

```go
package main
import "fmt"
func main() {
    age := map[string]int {"max": 24, "tom": 28}
    fmt.Println("map:", age)
    m := make(map[string]float64)
    m["E"]  = 2.7182818284
    m["Pi"] = 3.1415926535
    m["Phi"]= 1.6180339887

    for key, v := range m {
        fmt.Printf("Key: %v, Value: %v, Value: %v \n", key, v, m[key])
    }
    delete(m, "E") // does not return anything. It does nothing, if the key does not exist.
    fmt.Println("len:", len(m))
    fmt.Println("map:", m)

    _, ok := m["E"] // does the key exists?
    fmt.Println("ok:", ok)
}
```

Le parcours d'une map via un range n'est pas déterministe.
