# Go - first steps

- [Go - first steps](#go---first-steps)
  - [Ressources](#ressources)
  - [Hello world !](#hello-world-)


## Ressources

- https://ida.interchain.io/tutorials/4-golang-intro/1-install.html

## Hello world !

- Tous les programmes Go, s'exécutent dans des packages.
- Le package 'fmt' permet de formater le texte + IO
- La fonction par laquelle le programme va s'exécuter doit être 'main'

Créer un fichier hello.go

```go
package main
import "fmt"
func main() {
    fmt.Printf("Hello, World!\n")
}
```

Il faut à présent le compiler

```shell
# crée un fichier .mod
go mod init hello

# crée un fichier exécutable hello
go build
```

On peut maintenant exécuter le programme

```shell
./hello
```
