# Packages

- [Packages](#packages)
  - [Ressources](#ressources)
  - [Errors](#errors)
  - [fmt -\> format](#fmt---format)
    - [Ecoute entrée utilisateur](#ecoute-entrée-utilisateur)
    - [Calcul \& transtypage](#calcul--transtypage)
  - [strconv -\> string convertor](#strconv---string-convertor)
    - [string =\> entier](#string--entier)
    - [xxx -\> string](#xxx---string)
  - [Convention de nomage](#convention-de-nomage)

## Ressources

- [Interchain Academy](https://ida.interchain.io/tutorials/4-golang-intro/6-packages.html)
- [Tour de Go](https://go.dev/tour/welcome/1)

## Errors

```go
package main

import (
    "errors"
    "fmt"
)

func div(a, b float64) (float64, error) {
    if b == 0 {
        return -1, errors.New("can't perform division by zero")
    }
    return a / b, nil
}

func main() {
    for i := 2.; i >= -2; i = i - 0.5 {
        if x, err := div(3, i); err != nil {
            fmt.Println(err)
        } else {
            fmt.Printf("3/%v=%v\n", i, x)
        }
    }
}

/* => output

3/2=1.5
3/1.5=2
3/1=3
3/0.5=6
can't perform division by zero
3/-0.5=-6
3/-1=-3
3/-1.5=-2
3/-2=-1.5
*/
```

## fmt -> format

### Ecoute entrée utilisateur

Il est possible de capturer dans une variable la donnée entré par l'utilisateur avec la bibliothèque 'bufio' et sa fonction 'NewScanner()'

```go
package main

import (
    "bufio"
    "fmt"
    "os"
)

func main() {
    // création du scanner capturant une entrée utilisateur
    scanner := bufio.NewScanner(os.Stdin) 
    fmt.Print("Entrez quelque chose : ")
    
    // lancement du scanner
    scanner.Scan()                      
    
    // stockage du résultat du scanner dans une variable
    entreeUtilisateur := scanner.Text() 
    fmt.Println(entreeUtilisateur)
}
```

### Calcul & transtypage

En GO, il n'est pas possible de faire calcul entre deux variables de types différents :

```go
package main

import "fmt"

func main() {
    var x int = 50
    var y float32 = 30.5

    // Ceci renverra -> invalid operation: x + y (mismatched types int and float32)
    fmt.Printf("x + y = ", x+y) // addition d'une variable de type int et et une autre de type float32
}
```

Il y a deux solutions à ce problème : 1 - typer dès le début ou alors caster au moment du calcul :

```go
package main

import "fmt"

func main() {
    var x int = 50
    var y float32 = 30.5

    // Ceci renverra -> 80.5
    fmt.Printf("x + y = ", float32(x )+ y) // convertir le type de la variable x de int en float32
}
```

## strconv -> string convertor

### string => entier

Il est possible de caster un string en entier avec la fonction 'Atoi' de la bibliothèque 'strconv' :

```go
package main

import (
    "bufio"
    "fmt"
    "os"
    "strconv"
)

func main() {
    scanner := bufio.NewScanner(os.Stdin)
    fmt.Print("Entrez un nombre entier : ")
    scanner.Scan()

    nbr, _ := strconv.Atoi(scanner.Text()) // conversion du type string en int

    fmt.Printf("res : %d\n", (nbr + 6))
}
```

Ou encore

```go
package main
import (
    "fmt"
    "strconv"
)
func main() {
    v32 := "-354634382"
    if s, err := strconv.ParseInt(v32, 10, 32); err == nil {
        fmt.Printf("%T, %v\n", s, s)
    }
    if s, err := strconv.ParseInt(v32, 16, 32); err == nil {
        fmt.Printf("%T, %v\n", s, s)
    }
    v64 := "-3546343826724305832"
    if s, err := strconv.ParseInt(v64, 10, 64); err == nil {
        fmt.Printf("%T, %v\n", s, s)
    }
    if s, err := strconv.ParseInt(v64, 16, 64); err == nil {
        fmt.Printf("%T, %v\n", s, s)
    }
}
```

### xxx -> string

```go
// returns "true" or "false" according to b.
func FormatBool(b bool) string

// returns a string representation of f with format fmt and with precision prec.
func FormatFloat(f float64, fmt vyte, prec, bitSize int) string 

// returns the string representation of i in base base.
func FormatInt(i int64, base int) string

//is the same as FormatInt, but it takes a unsigned integer.
func FormatUint(i uint, base int) string

// is shorthand for FormatInt(int64(i), 10), so it gives a decimal representation of i as a string.
func Itoa(i int) string 
```

## Convention de nomage

Pour exporter une variable hors du package, il faut qu'elle commence par une majuscule.

```go
// un return sans argument, renvoie les params cités dans la signature
func split(sum int) (x, y int) {
 x = sum * 4 / 9
 y = sum - x
 return
}
```
