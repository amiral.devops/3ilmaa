# Manifeste

 

## Finalité du manifeste

***

C'est là la feuille de route à exécuter.

## Compétences à développer

***

- [ ] Front-end technologies
  - [ ] JavaScript
    - [x] [1- Apprenez à programmer avec JavaScript](https://openclassrooms.com/fr/courses/6175841-apprenez-a-programmer-avec-javascript)
    - [X] [2- Créer des pages Web dynamiques avec JavaScript ](https://openclassrooms.com/fr/courses/7697016-creez-des-pages-web-dynamiques-avec-javascript)
    - [X] [4- Tester vos applications front-end avec JavaScript](https://openclassrooms.com/fr/courses/7159306-testez-vos-applications-front-end-avec-javascript)


  - [ ] Node
    - [ ] [5- Passez au full stack avec Node.js Express & MongoDB](https://openclassrooms.com/fr/courses/6390246-passez-au-full-stack-avec-node-js-express-et-mongodb)


  - [ ] Angular
    - [X] [6- Débuter avec Angular](https://openclassrooms.com/fr/courses/7471261-debutez-avec-angular)
    - [X] [7- Completer vos connaissances sur Angular](https://openclassrooms.com/fr/courses/7471271-completez-vos-connaissances-sur-angular)
    - [ ] [8- Perfectionnez vous sur Angular](https://openclassrooms.com/fr/courses/7471281-perfectionnez-vous-sur-angular)
    - [x] [9- Testez une application Angular](https://www.youtube.com/playlist?list=PLckzBDi8b8tn-m414l0Hdoh0zNJta_bPa)
    - [ ] [10- les formulaires Angular](https://www.youtube.com/watch?v=OOiw9wuogK0&list=PLrbLGOB571zdODiE_hjUcHsSD4jzHnQ7t)
    - [ ] [11- Angular RxJS](https://www.youtube.com/watch?v=ESGnIwT5YdM&list=PLrbLGOB571zc3s8Mu4d1H-1PNzn6vc1Mb)


  - [ ] html / css
    - [ ] [3- Créez votre site Web avec HTML5 & CSS3](https://openclassrooms.com/fr/courses/1603881-creez-votre-site-web-avec-html5-et-css3)
    - [ ] [Créez des animations CSS modernes](https://openclassrooms.com/fr/courses/5919246-creez-des-animations-css-modernes)
    - [ ] [Découpez et intégrez une maquette](https://openclassrooms.com/fr/courses/3504431-decoupez-et-integrez-une-maquette)
    - [ ] [Codez un site accessible avec HTML & CSS](https://openclassrooms.com/fr/courses/6691451-codez-un-site-web-accessible-avec-html-css)


- [ ] Java 8/11
  - [ ] lambdas
  - [ ] classes/fonctions anonymes
  - [ ] collections
  - [ ] stream/collections

- [ ] Spring microservices
  - [ ] [Construisez des microservices](https://openclassrooms.com/fr/courses/4668056-construisez-des-microservices)
  - [ ] [Optimisez votre architecture mocroservices](https://openclassrooms.com/fr/courses/4668216-optimisez-votre-architecture-microservices)

- [ ] Devops culture
  - [ ] [Découvrez la méthodologie DevOps](https://openclassrooms.com/fr/courses/6093671-decouvrez-la-methodologie-devops)
  - [ ] [Mettez en place l'intégration et la livraison continues avec la démarche DevOps](https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops)
  - [ ] [Optimisez votre déploiement en créeant des conteneurs avec Docker](https://openclassrooms.com/fr/courses/2035766-optimisez-votre-deploiement-en-creant-des-conteneurs-avec-docker)
  - [ ] [Utilisez Ansible pour automatiser vos tâches de configuration](https://openclassrooms.com/fr/courses/2035796-utilisez-ansible-pour-automatiser-vos-taches-de-configuration)

- [ ] Cloud AWS
    - [ ] [Découvrez le cloud avec AWS](https://openclassrooms.com/fr/courses/4810836-decouvrez-le-cloud-avec-amazon-web-services)
    - [ ] [Déployez vos systèmes & réseaux dans le cloud avec AWS](https://openclassrooms.com/fr/courses/2035756-deployez-vos-systemes-et-reseaux-dans-le-cloud-avec-aws)
    - [ ] [Devenez un architecte de solutions AWS](https://openclassrooms.com/fr/courses/7818631-devenez-un-architecte-de-solutions-aws)
- [ ] Management
  - [ ] Agile
  - [ ] Scrum
  - [ ] SAFe
- [ ] Algo & structures de données
- [ ] ElastikSearch
- [ ] Kotlin
- [ ] GO
- [ ] Blockchain
  - [ ] Cosmos SDK
  - [ ] EVM
  - [ ] Rust
  - [ ] WebAssembly

